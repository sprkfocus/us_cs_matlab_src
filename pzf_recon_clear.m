function [slice, used_data_ratio] = pzf_recon_clear( do_cs )
% function straightpath_congd20151021_3
        lamda= 3.66756e-09;          
        lamda_tv=8.15307e-07;  
        t_step_init = 1;
    
    include_subpaths();
    data_dir = 'sim_results/72__/';
    Length= 4e-2;    
    Width=  4e-2;   
    
    outmaxiter = 25;

    m=65; n=65;                     
          
    d_x=Length/(n-1);d_y=Width/(m-1);

    
    load([data_dir 'sensor_pos.mat']);
    sensors_pos = sensors_pos_abs2grid(sensors_pos, Length, Width, m, n);
% %     sensors_pos = Bt1;
% %     clearvars sensors;
%     sensors_pos(1,:) = (sensors_pos(1,:) + Length/2) / d_x;
%     sensors_pos(2,:) = (sensors_pos(2,:) + Width/2) / d_y;
%     s_min = min(sensors_pos(:)); s_max = max(sensors_pos(:));
%     scale_f = @(x) (x - s_min) / (s_max - s_min) * ((m - 1) - 1) + 1;
%     sensors_pos = arrayfun(scale_f, sensors_pos);    
    
    load([data_dir 'arrivals_t.mat']);
    arrival_t = T0;
    
    load('str_9_path_65_effective.mat');
    G0 = sparse(L_3_effective);
 
 
    c=1500;                          
    s0=1/c;
    
        
    N=3196; %  effective cells
    SS0=s0.*ones(N,1);  

    load('cached/psi_3196.mat'); % psi_=orth(rand(3196, 3196));

    Tol_grad=1e-16;    

    sigma=zeros(N);      
    iter_cond=1;                
    alpha=0.01;beta=0.5;  
    t_=t_step_init;                
    n0=0;                

    load('helpers/SS_res.mat');  
    tx_num = 72;
    rx_num = 9;
    
     
    M = (m - 1)*(n - 1);
    
    if do_cs
        cs_M = 72*9;
        cs_m = ceil(0.90 * cs_M);   
        
        used_data_ratio = cs_m / cs_M;
        
        cs_A = sparse(cs_m, cs_M);
        rng('default'); rng(2016);

        equations_in_use = randperm(cs_M);
        for i = 1 : cs_m
            line_idx = equations_in_use(i);
            cs_A(i, line_idx) = 1; 
        end

        G0 = sparse(cs_A * G0);
       
       arrival_t = cs_A * arrival_t;
       disp(['Data in use: ' int2str(cs_m) ' of ' int2str(cs_M) ' = ' num2str(cs_m / cs_M * 100) '%']);
    else
        used_data_ratio = 1.0;
    end

    while (iter_cond)
        disp(['reconstruct iter: ' int2str(n0) ' / ' int2str(outmaxiter) ]);  
        SS_cur = SS0;      
        psiT_slow = psi_' * SS_cur; 

        for ii=1:N
            sigma(ii,ii)=sqrt((psiT_slow(ii))^2+eps);
        end
        
%         if (n0 == 0 && load_paths)
%             load('temp_G0.mat');
%         elseif ( n0 == 0 )
%             disp('start raylength matrix calculating...'); 
%             calc_start = cputime();
%             slowness_map = ones(M, 1).*s0;  %./100;
%             num_SS_res=numel(SS_res);
%             for jj=1:num_SS_res
%                 slowness_map(SS_res(jj))=SS0(jj);
%             end
%             dx = Length / (m - 2);
%             G_t = calc_Raylength_tb_eval(slowness_map, sensors_pos, rx_indexs_adv, dx, dx);
%             G0 = zeros(size(G_t, 1), N);
%             for p_id = 1 : size(G_t, 1)
%                 for jj=1 : num_SS_res
%                     G0(p_id, jj) = G_t(p_id, SS_res(jj));
%                 end
%             end
%             if n0 == 0 && ~load_paths
% %                save('temp_G0.mat', 'G0'); disp('G0[0] has been saved'); 
%             end
%             disp([' > raylength matrix is ready! Elapsed: ' num2str(cputime() - calc_start) '[s]']);
%         end

        if n0 == 0
            G_prev = G0;
        end



%         C0(:,n0+1)=norm((G0*SS(:,n0+1)-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS(:,n0+1));  %Ŀ�꺯��
        r0=G_prev * SS_cur-arrival_t;                  
        grad_C0=2*G0'*r0+lamda*psi_*1/(sigma)*psiT_slow+lamda_tv*grad_1norm_tv(SS_cur);    %Ŀ�꺯����ݶ�
        grad_C1=grad_C0;              %grad_C1���ÿ�ε��ݶ�


        if  n0==0
            gamma_1=0;                                  %gamma_1������еĲ���
            delta_m_1=-grad_C0;                 %delta_m_1�����

        else
    %         gamma_1 = norm(grad_C0(:, n0 + 1) ,2 )^2 / norm(grad_C0(:, n0) , 2)^2;
            yta = grad_C1-grad_C0_prev;
            gamma_1=max(0,min((grad_C1'*yta)/(delta_m_0'*yta),norm(grad_C1,2)^2/(delta_m_0'*yta)));
            delta_m_1=-grad_C1+gamma_1*delta_m_0;
        end

           %��backline search Ѱ�Ҳ���u_n
        while (norm((G0*(SS_cur+t_*delta_m_1)-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS_cur))>...
               (norm((G0*SS_cur-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS_cur)+alpha*t_*real(grad_C1'*delta_m_1))
           t_=beta*t_;
        end

        u_n0=t_;
        if u_n0 < 1
           disp(['mu =' num2str(u_n0)]) 
        end
        SS1=SS0+u_n0*delta_m_1;                 
        f=norm(u_n0*delta_m_1,2)/norm(SS0,2);   

        if (f>Tol_grad)&&(n0<outmaxiter)
           iter_cond=1;
        else
           iter_cond=0;
        end

        G_prev = G0;
        grad_C0_prev = grad_C0;
        delta_m_0=delta_m_1;
        SS0=SS1;
        n0=n0+1;
        t_=t_step_init;

    end

    SS_effective=SS1;

    num_SS_res=numel(SS_res);
    SS_final=s0*ones(M,1);
    for jj=1:num_SS_res
       SS_final(SS_res(jj))=SS_effective(jj);
    end

     
   VDOTMN=zeros(m,n);  
   
    for k=1:M
        I=floor(k/(n-1));
        J=mod(k,(n-1));
        if (I~=0)&&(J~=0)
            VDOTMN(I:I+1,J:J+1)=1/SS_final(k);
        elseif(I==0)&&(J~=0)
            VDOTMN(1:2,J:J+1)=1/SS_final(k);    
        elseif(I~=0)&&(J==0)
            VDOTMN(I:I+1,n-1:n)=1/SS_final(k);
        end
    end
           
    slice = VDOTMN(1:64,1:64);
end


          
          
