% x: lamda , lamda_tv, t_step

x0 = [ 1.6e-09  ];
lb = [ 1e-10 ];
ub = [ 1e-8 ];

no_plot_opts = saoptimset('TimeLimit', 1800,   ...
    'StallIterLimit', 20, ...
    'ReannealInterval', 25, 'Display', 'iter',...
    'MaxIter', 25);

opts = saoptimset(no_plot_opts, 'PlotFcns',{@saplotf, @saplotbestf, @saplottemperature, @saplotstopping});

[x, fval, exitflag, output] = simulannealbnd(@objfun_cs, x0, lb, ub, opts);

disp(['best x = ' num2str(x)]);

% CS all ones :9.3721e-08  > 214.7890
% CS iid ones :7.5068e-08
% CS all cs : 3.786e-08 > 177
% CS iid cs : 3.786e-08 > 182
% CS no-line-comb cs : 

