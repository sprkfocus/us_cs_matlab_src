function [ result ] = obj_cost_function( x )
disp(['c lambda, lamda_tv, t_init =' num2str(x)])
% x description: x1 - lamda, x2 -lamda_tv, x3- backline search step

    speed_model = simulated_model_1();
    cs_m = 4 * 12^2 * log(64^2 / 12^2);
    slice = reconstruct_cs_classic( cs_m, false,  x(1), x(2), x(3) ); 
    
%     [psnr_error, snr_error] = psnr(slice, speed_model);
%     result = 1/snr_error; % because we want to minimize cost function
    
    result = norm(speed_model - slice, 'fro');  % this is ok

end

