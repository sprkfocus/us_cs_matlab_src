% function [ result ] = probe_clear(t_shift, r_threshold, lamda, lamda_tv )
function [ result ] = probe_clear(lamda )
    disp('--------------------------------------------------------------------------------');
    disp(['lamda: ' num2str(lamda)]);

% -------------------------------------------------------------------------
% make reference
    speed_model = simulated_model_1();

%     N = 64;
% %     z_i = 3;
%     z_steps = 15;
%     side_len = 6e-2;
%     height_len = 2.5e-2;
%     z_step_len = height_len / z_steps;
%     z_top = -1e-2;
%     dx = side_len / N;
%     dy = side_len / N;
%     
%     % t1  pos
%     radius1 = 3.75 * 1e-3; % [m]
%     c1_x = floor(-1e-2 /dx + N/2);
%     c1_y = floor(-1e-2 /dy + N/2);
%     c1_zp = -0.5e-2;
%     % t2
%     radius2 = 2.5 * 1e-3; % [m]
%     c2_x = floor(1.5e-2 /dx + N/2);
%     c2_y = floor(1.5e-2 /dy + N/2);
%     c2_zp = 1e-2;
%     if load_model
%         load('X_model.mat');
%     else
%         z_in = z_i;
%         X_model = zeros(N,N,z_steps);
%         for z_i = 1:15
%             z_pos = z_top + z_step_len * z_i;
% 
%             speed = zeros(N,N);
%             [IH, IW] = ndgrid(1:N, 1:N);
%             tumor_model = zeros(N,N); tumor_model = tumor_model > 0;
%             % tumor 1 radius 
%             d_centre1 = abs(z_pos - c1_zp);
%             if d_centre1 < radius1
%                r1_shifted = (radius1^2 - d_centre1^2)^0.5 /dx;
%                d = ((IH-c1_x)./r1_shifted).^2 + ((IW-c1_y)./r1_shifted).^2;
%                tumor1 = d < 1.0;
%                tumor_model = tumor_model | tumor1;
%             end
% 
%             % tumor 2 
%             d_centre2 = abs(z_pos - c2_zp);
%             if d_centre2 < radius2
%                r2_shifted = (radius2^2 - d_centre2^2)^0.5 /dx;
%                d = ((IH-c2_x)./r2_shifted).^2 + ((IW-c2_y)./r2_shifted).^2;
%                tumor2 = d < 1.0;
%                tumor_model = tumor_model | tumor2;
%             end
% 
%             % figure, imagesc(tumor_model);
% 
%             water = ~tumor_model;
%             speed(water) = 1500;   
%             speed(tumor_model) = 2600;
%             X_model(:,:,z_i) = speed;
%         end
%         z_i = z_in;
%         save('X_model.mat', 'X_model');
%     end
%     speed = X_model(:, :, z_i);
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% -- recalculate arrivals for rx = 25 /100
%     arrival_time(0, 0, false, t_shift, r_threshold);

% -------------------------------------------------------------------------
% 
%     cs_s = 11^2;
%     cs_N = 64^2;
%     cs_m = 4 * cs_s * log(cs_N / cs_s);
%     [slice, used_data_ratio] = reconstruct_cs_classic(round(cs_m), false, lamda, lamda_tv, 1, true);

    slice = reconstruct_4096(false, [], false, lamda); 

% -------------------------------------------------------------------------
% obj fun to minimize
    
%     result =  norm(speed_model - slice, 'fro') % norm(speed - slice, 2);
    result = RMSE(slice, speed_model);

end

