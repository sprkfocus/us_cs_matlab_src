% if isempty(strfind(path,'../time_picker'))
%     addpath('../time_picker');
%     addpath('../helpers');
%     addpath('../modeling');
%     addpath('..');
% end



x0 = [ 1e-09  ];
lb = [ 1e-12 ];
ub = [ 1e-7 ];

no_plot_opts = saoptimset('TimeLimit', 36000,   ...
    'StallIterLimit', 65, ...
    'ReannealInterval', 60, 'Display', 'iter',...
    'MaxIter', 125);

opts = saoptimset(no_plot_opts, 'PlotFcns',{@saplotf, @saplotbestf, @saplottemperature, @saplotstopping});

[x, fval, exitflag, output] = simulannealbnd(@objfun_r,  x0, lb, ub, opts);

disp(['best x = ' num2str(x)]);

% Stop requested.
% best x = 4.6616e-08