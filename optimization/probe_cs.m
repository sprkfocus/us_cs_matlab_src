function [ result ] = probe_cs(lamda )
    disp('--------------------------------------------------------------------------------');
    disp([ '    lamda: ' num2str(lamda)  ]);

% -------------------------------------------------------------------------
% make reference
    speed_model = simulated_model_1();
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------

    cs_s = 11^2;
    cs_N = 64^2;
    cs_m = 4 * cs_s * log(cs_N / cs_s);
    [slice, time_e] = reconstruct_cs_classic('no_line_comb', 'cs', round(cs_m), false, lamda, 0, false);


% -------------------------------------------------------------------------
% obj fun to minimize
    
%     subslice_center = slice(29:35, 29:35);
%     subslice_left = slice(:, 1:20);
%     subslice_bottom = slice(44:64, :);
%     
%     submodel_center = speed_model(29:35, 29:35);
%     submodel_left = speed_model(:, 1:20);
%     submodel_bottom = speed_model(44:64, :);
%     
%     
%     er_center= RMSE(subslice_center, submodel_center);
%     er_left = RMSE(subslice_left, submodel_left);
%     er_bottom = RMSE(subslice_bottom, submodel_bottom);
% 
%     result = (er_left + 2 * er_center + er_bottom) / 4;.
    result = RMSE(slice, speed_model);

end

