function [ slice, effective ] = process_slice_param( arrival_t, rx_indexs, tx_idx, sensors_pos, lamda, lamda_tv, outmaxiter )
% function straightpath_congd20151021_3

    m=65; n=65;                     
    Length= 6e-2;    
    Width=  6e-2;             
    d_x=Length/(n-1); d_y=Width/(m-1);
    M=(m-1)*(n-1);    

    c=1500;                          
    s0=1/c;
    load_paths= false && 2 == exist('temp_G0.mat', 'file');

    N=3196; %  effective cells
    SS0=s0.*ones(N,1);  

    load('psi3196_.mat'); % psi_=orth(rand(3196, 3196));

    Tol_grad=1e-16;    

    sigma=zeros(N);      
    iter_cond=1;                
    alpha=0.01;beta=0.5;  
    t_=1;                
    n0=0;                


    load('mask.mat');
    load('SS_res.mat');  
    tx_num = size(rx_indexs, 1);
    rx_num = size(rx_indexs, 2);


    while (iter_cond)
        disp(['reconstruct iter: ' int2str(n0) ' / ' int2str(outmaxiter) ]);  
        SS_cur = SS0;      
        psiT_slow = psi_' * SS_cur; 

        for ii=1:N
            sigma(ii,ii)=sqrt((psiT_slow(ii))^2+eps);
        end

        if n0 == 0 && load_paths
            load('temp_G0.mat');
            if size(G0, 1) == tx_num * rx_num && size(G0, 2) == N
                disp('raylength matrix has been loaded');
            else
               clearvars G0;  
            end
        end
        if ( n0 <= 1 && (~load_paths || ~exist('G0', 'var')) )
            slowness_map = ones(M, 1).*s0;  %./100;
            num_SS_res=numel(SS_res);
            for jj=1:num_SS_res
                slowness_map(SS_res(jj))=SS0(jj);
            end
            G_t = calc_Raylength_tb(slowness_map, sensors_pos, tx_idx, rx_indexs, d_x, d_y);
            G0 = zeros(tx_num * rx_num, N);
            for p_id = 1 : tx_num * rx_num
                for jj=1 : num_SS_res
                    G0(p_id, jj) = G_t(p_id, SS_res(jj));
                end
            end
%             save('temp_G0.mat', 'G0'); disp('G0 saved');
        else
            
            
        end

        if n0 == 0
            G_prev = G0;
        end



%         C0(:,n0+1)=norm((G0*SS(:,n0+1)-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS(:,n0+1));  %Ŀ�꺯��
        r0=G_prev * SS_cur-arrival_t;                  
        grad_C0=2*G0'*r0+lamda*psi_*1/(sigma)*psiT_slow+lamda_tv*grad_1norm_tv(SS_cur);    %Ŀ�꺯����ݶ�
        grad_C1=grad_C0;              %grad_C1���ÿ�ε��ݶ�


        if  n0==0
            gamma_1=0;                                  %gamma_1������еĲ���
            delta_m_1=-grad_C0;                 %delta_m_1�����

        else
    %         gamma_1 = norm(grad_C0(:, n0 + 1) ,2 )^2 / norm(grad_C0(:, n0) , 2)^2;
            yta = grad_C1-grad_C0_prev;
            gamma_1=max(0,min((grad_C1'*yta)/(delta_m_0'*yta),norm(grad_C1,2)^2/(delta_m_0'*yta)));
            delta_m_1=-grad_C1+gamma_1*delta_m_0;
        end

           %��backline search Ѱ�Ҳ���u_n
        while (norm((G0*(SS_cur+t_*delta_m_1)-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS_cur))>...
               (norm((G0*SS_cur-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS_cur)+alpha*t_*real(grad_C1'*delta_m_1))
           t_=beta*t_;
        end

        u_n0=t_;
        if u_n0 < 1
           disp(['mu =' num2str(u_n0)]) 
        end
        SS1=SS0+u_n0*delta_m_1;                 
        f=norm(u_n0*delta_m_1,2)/norm(SS0,2);   

        if (f>Tol_grad)&&(n0<outmaxiter)
           iter_cond=1;
        else
           iter_cond=0;
        end

        G_prev = G0;
        grad_C0_prev = grad_C0;
        delta_m_0=delta_m_1;
        SS0=SS1;
        n0=n0+1;
        t_=1;

    end

    SS_effective=SS1;

    num_SS_res=numel(SS_res);
    SS_final=s0*ones(M,1);
    for jj=1:num_SS_res
       SS_final(SS_res(jj))=SS_effective(jj);
    end



     
   VDOTMN=zeros(m,n);  
   
    for k=1:M
        I=floor(k/(n-1));
        J=mod(k,(n-1));
        if (I~=0)&&(J~=0)
            VDOTMN(I:I+1,J:J+1)=1/SS_final(k);
        elseif(I==0)&&(J~=0)
            VDOTMN(1:2,J:J+1)=1/SS_final(k);    
        elseif(I~=0)&&(J==0)
            VDOTMN(I:I+1,n-1:n)=1/SS_final(k);
        end
    end
           
    effective = SS_effective .^(-1);
    slice = VDOTMN(1:64,1:64);
end

