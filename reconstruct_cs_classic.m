function [slice, time_elapsed] = reconstruct_cs_classic(mrand_type, ...
            fill_val, cs_m , do_bigdata, lamda, lamda_tv, do_use_temp_data)
        
    include_subpaths();
    if exist('do_use_temp_data', 'var') && do_use_temp_data
        data_prefix = 'temp_';
    else
        data_prefix = '';
    end
    if exist('do_bigdata', 'var') && do_bigdata
        data_dir = 'sim_results/1024/';
    else
        data_dir = 'sim_results/wave_s1/';
        do_bigdata = false;
    end

    load([data_dir data_prefix 'arrivals_t.mat'], 'rx_indexs');

    tx_num = size(rx_indexs, 1);
    rx_num = size(rx_indexs, 2);
    
    %---------- randomization --------------------------------------------
    cs_M = tx_num * rx_num;
    if ~exist('cs_m', 'var')
        cs_m = ceil(0.65 * cs_M);
        warning([' > setting cs_m = ' int2str(cs_m)]);
    else
        cs_m = round(cs_m);
        disp([' > using cs_m = ' int2str(cs_m)]);
    end
    if cs_m > cs_M
        error('Error using CS: required amount of the data is bigger than provided');
    end
  
    % fill A matrix
        
    [cs_A, used_lines] = construct_cs_matrix(cs_m, cs_M, mrand_type, fill_val, rx_num);
    disp(['Data volume in use: ' int2str(used_lines) '/' int2str(cs_M) '  = ' num2str(100* used_lines/cs_M) '%']);
        
    %----------------------------------------------------------------------
    
    tic();
    if exist('lamda', 'var')
       [slice, cost_seq, rmse_seq] = reconstruct_4096(do_bigdata, cs_A, ...
                                false, lamda, lamda_tv, do_use_temp_data);
    else   
       [slice, cost_seq, rmse_seq] = reconstruct_4096(do_bigdata, cs_A, false); 
    end
    time_elapsed = toc();
end


