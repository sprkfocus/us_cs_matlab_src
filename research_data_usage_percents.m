% Research script to show data usage efficiency

speed_model = simulated_model_1();
N_side = size(speed_model, 1);

cs_M_kwave_ext = 2500;
% cs_M_pzf = 9 *72;


percents = 10 : 5 : 100;
% percents = 60 : 1 : 75;
used_data_ratio = percents ./ 100.0;
runs_num = size(percents, 2);

speedmap_kwave = zeros(N_side, N_side, runs_num);
% speedmap_pzf = zeros(N_side, N_side, runs_num);

% --- relative to the model
snr_seq = zeros(runs_num, 1);
rmse_seq = zeros(runs_num, 1);

norm_fro = zeros(runs_num, 1);

% snr_pzf = zeros(runs_num, 1);
% norm_fro_pzf = zeros(runs_num, 1);

% --- relative to full reconstruction
snr_kwave_tofull = zeros(runs_num, 1);
rmse_tofull_seq = zeros(runs_num, 1);
norm_fro_tofull = zeros(runs_num, 1);

% snr_pzf_tofull = zeros(runs_num, 1);
% norm_fro_pzf_tofull = zeros(runs_num, 1);

time_elapsed_kwave = zeros(runs_num, 1);
% time_elapsed_pzf = zeros(runs_num, 1);

basic_slice_file = 'cached/basic_slice.mat';
if true || ~exist(basic_slice_file, 'file')
    slice_kwave_clear = reconstruct_4096();
    save(basic_slice_file, 'slice_kwave_clear');
else
    load(basic_slice_file);
end
% slice_pzf_clear = pzf_recon_clear(false); 


for i = 1 : runs_num
    disp(['Percent of data: ' int2str(percents(i))]);
    
    ratio = used_data_ratio(i);
    cs_m_kwave = round(ratio * cs_M_kwave_ext);

    % kwave
    [slice, elapsed] = reconstruct_cs_classic('no_line_comb', 'cs', cs_m_kwave);

    speedmap_kwave(:,:,i) = slice;
    time_elapsed_kwave(i) = elapsed;
    
    rmse_seq(i) = RMSE(slice, speed_model);
    rmse_tofull_seq(i) = RMSE(slice, slice_kwave_clear);
        
    snr_seq(i) = SNR(slice, speed_model);
    snr_kwave_tofull(i) = SNR(slice, slice_kwave_clear);
    
    norm_fro(i) = norm(slice - speed_model, 'fro');
    norm_fro_tofull(i) = norm(slice - slice_kwave_clear, 'fro');

    % --

    % pzflex
%     tic();
%     [slice, dummy] = pzf_recon_clear_percent(ratio);
%     elapsed = toc();
%     speedmap_pzf(:,:,i) = slice;
%     time_elapsed_pzf(i) = elapsed;
%     [dummy, snr_pzf(i)] = psnr(slice, speed_model);
%     norm_fro_pzf(i) = norm(slice - speed_model, 'fro');
%     [dummy, snr_pzf_tofull(i)] = psnr(slice, slice_pzf_clear);
%     norm_fro_pzf_tofull(i) = norm(slice - slice_pzf_clear, 'fro');

end

% save('workspaces/percentage_research_single-ones.mat', 'snr_seq', 'rmse_seq', 'snr_kwave_tofull', 'rmse_tofull_seq');

figure;
subplot(231); plot(percents, rmse_seq); title('rmse seq');

subplot(233); plot(percents, snr_seq); title('snr seq');

subplot(232); plot(percents, norm_fro); title('norm seq');

subplot(234); plot(percents, rmse_tofull_seq); title('rmse tofull');

subplot(236); plot(percents, snr_kwave_tofull); title('snr tofull');

subplot(235); plot(percents, norm_fro_tofull); title('norm tofull');

% save('percentage_research.mat', 'speedmap_kwave', 'speedmap_pzf', ...
%     'snr_kwave', 'snr_pzf', 'norm_fro_kwave', 'norm_fro_pzf', ...
%     'snr_kwave_tofull' , 'snr_pzf_tofull', 'norm_fro_kwave_tofull', 'norm_fro_pzf_tofull', ...
%     'time_elapsed_kwave', 'time_elapsed_pzf');



