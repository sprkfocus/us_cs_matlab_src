function [slice, used_data_ratio] = reconstruct_cs_trial( cs_m , do_bigdata, lamda, lamda_tv, t_init)
    include_subpaths();
    if exist('do_bigdata', 'var') && do_bigdata
        data_dir = 'sim_results/1024/';
    else
        data_dir = 'sim_results/wave_s1/';
        do_bigdata = false;
    end

    load([data_dir 'arrivals_t.mat'], 'rx_indexs');

    tx_num = size(rx_indexs, 1);
    rx_num = size(rx_indexs, 2);
    
    %---------- randomization --------------------------------------------
    cs_M = tx_num * rx_num;
    if ~exist('cs_m', 'var')
        cs_m = ceil(0.65 * cs_M);
        warning([' > setting cs_m = ' int2str(cs_m)]);
    else
        cs_m = round(cs_m);
        disp([' > using cs_m = ' int2str(cs_m)]);
    end
    if cs_m > cs_M
        error('Error using CS: required amount of the data is bigger than provided');
    end
    
    used_data_ratio = cs_m / cs_M;
    disp(['Data in use: ' int2str(cs_m) ' of ' int2str(cs_M) ' = ' num2str(used_data_ratio * 100) '%']);
    
    rng('default'); rng(2016);

    equations_in_use = randperm(cs_M);
    equations_in_use = equations_in_use(1 : cs_m);
    equations_in_use = sort(equations_in_use);
    
    % fill A matrix
    cs_A = sparse(cs_m, cs_M);
    for i = 1 : cs_m
        line_idx = equations_in_use(i);
        cs_A(i, line_idx) = 1;        
    end
    
    rx_indexs_cs_adv = rx_index_full2sifted_adv(rx_indexs, equations_in_use);
    
    %----------------------------------------------------------------------
    
   if exist('lamda', 'var')
       slice = reconstruct_clear_trial(do_bigdata, rx_indexs_cs_adv, cs_A, ...
                                false, lamda, lamda_tv, t_init);
   else   
       slice = reconstruct_clear_trial(do_bigdata, rx_indexs_cs_adv, cs_A, false); 
   end


end


