function [ speed_model ] = simulated_model_1( )
% -------------------------------------------------------------------------
% make reference
    N = 64;

    side_len = 4e-2;
    
    dx = side_len / N;
    dy = side_len / N;
    
    % t1  pos
    radius1 = 2.5 * 1e-3; % [m]
    c1_x = floor(0e-2 /dx + N/2);
    c1_y = floor(0e-2 /dy + N/2);

    % t2
    radius2 = 0.75 * 1e-3; % [m]
    c2_x = floor(7.25e-3 /dx + N/2);
    c2_y = floor(0 /dy + N/2);

    speed = zeros(N,N);
    [IH, IW] = ndgrid(1:N, 1:N);
    tumor_model = zeros(N,N); tumor_model = tumor_model > 0;

    d = ((IH-c1_x)./ (radius1/dx) ).^2 + ((IW-c1_y)./ (radius1/dx) ).^2;
    tumor1 = d < 1.0;
    tumor_model = tumor_model | tumor1;


    % tumor 2 

    d = ((IH-c2_x)./radius2).^2 + ((IW-c2_y)./radius2).^2;
    tumor2 = d < 1.0;
    tumor_model = tumor_model;


    % figure, imagesc(tumor_model);

    water = ~tumor_model;
    speed(water) = 1500;   
    speed(tumor_model) = 2600;
    
    speed_model = speed;
end

