function [ speed_model ] = simulated_model_bigdata( )
% -------------------------------------------------------------------------
% make reference
  side_len = 0.2; % [m]
    N = 64;
%     N_clear = N - 22*2;
    dx = side_len / N;
    dy = dx;

    speed_model = zeros(N,N);

    [IH,IW] = ndgrid(1:N,1:N);
    % tumor 1 
    radius1 = 10 * 1e-3; % [m]
    r1 = radius1 / dx;
    c1_x = floor(-30e-3 /dx + N/2);
    c1_y = floor(-30e-3 /dy + N/2);
    d = ((IH-c1_y)./r1).^2 + ((IW-c1_x)./r1).^2;
    tumor1 = d < 1.0;

    % tumor 2
    radius2 = 5 * 1e-3  / dx;
    c2_x = floor(15 * 1e-3 / dx + N/2);
    c2_y = floor(15e-3 /dy + N/2);
    d = ((IH-c2_y)./radius2).^2 + ((IW-c2_x)./radius2).^2;
    tumor2 = d < 1.0;

    % tumor 3
    radius3 = 2.5 * 1e-3  / dx;
    c3_x = floor(55 * 1e-3 / dx + N/2);
    c3_y = floor(-15e-3 /dy + N/2);
    d = ((IH-c3_y)./radius3).^2 + ((IW-c3_x)./radius3).^2;
    tumor3 = d < 1.0;


    tumor_model = tumor1 | tumor2 | tumor3;

    % figure, imagesc(tumor_model);
    
    speed_model(~tumor_model) = 1500;            % [m/s]
    speed_model(tumor_model) = 2600;
    
end

