function exitcode = simulation( start_idx, end_idx )
    start_idx = str2num(start_idx);
    end_idx = str2num(end_idx);
    exitcode = 1;
    mkdir('sim_res');
    nSource = 100;
    mkdir(['sim_res/' int2str(nSource)]);

    %medium.alpha_mode = 'no_dispersion';
    disp('Starting overall simulation...');


    save_dir = ['sim_res/' int2str(nSource)];

    side_len = 4e-2; % [m]
    N = 256;
    N_clear = N - 22*2;
    dx = side_len / N_clear;
    dy = dx;

    speed = zeros(N,N);
    density = zeros(N,N);
    alpha_power = zeros(N,N);
    alpha_coeff = zeros(N,N);
    BonA = zeros(N,N);

    S = zeros(N,N);

    [IH,IW] = ndgrid(1:N,1:N);
    % tumor 1 
    radius1 = 2.5 * 1e-3; % [m]
    r1 = radius1 / dx;
    c1_x = floor( 0e-3 /dx + N/2);
    c1_y = floor( 0e-3 /dy + N/2);
    d = ((IH-c1_y)./r1).^2 + ((IW-c1_x)./r1).^2;
    tumor1 = d < 1.0;

    % tumor 2
    radius2 = 0.725 * 1e-3  / dx;
    c2_x = floor( 7.25 * 1e-3 / dx + N/2);
    c2_y = floor( 0e-3 /dy + N/2);
    d = ((IH-c2_y)./radius2).^2 + ((IW-c2_x)./radius2).^2;
    tumor2 = d < 1.0;

    % tumor 3
%     radius3 = 2.5 * 1e-3  / dx;
%     c3_x = floor(55 * 1e-3 / dx + N/2);
%     c3_y = floor(-15e-3 /dy + N/2);
%     d = ((IH-c3_y)./radius3).^2 + ((IW-c3_x)./radius3).^2;
%     tumor3 = d < 1.0;


    tumor_model = tumor1 | tumor2 ; %| tumor3;

    % figure, imagesc(tumor_model);

    water = ~tumor_model;

    medium = [];



    speed(water) = 1500;            % [m/s]
    alpha_coeff(water) = 0.002;     % [dB/(MHz^y cm)]
    alpha_power(water) = 1.2;
    density(water) = 1000;
    BonA(water) = 6.0;

    speed(tumor_model) = 2600;
    alpha_coeff(tumor_model) = 0.75;
    alpha_power(tumor_model) = 1.5;
    density(tumor_model) = 1700;
    BonA(tumor_model) = 8.0;


    medium.sound_speed = reshape(speed,[N N]);
    medium.sound_speed_ref = 1460;
    medium.density = reshape(density,[N N]);
    medium.alpha_coeff = reshape(alpha_coeff,[N N]);
    medium.alpha_power = 1.5;
    medium.BonA = reshape(BonA,[N N]);

    kgrid_base = makeGrid(N, dx, N, dy);

    sensor_radius = (N/2-22)*dx;
    sensors = makeCartCircle(sensor_radius, nSource);
    
    for i = start_idx : end_idx 
        disp(['Iteration: ' int2str(i) '/' int2str(end_idx) ]);
        tic;
        kgrid = kgrid_base;
        m = medium;
        source = [];
        sensor = [];
        sensor.mask = sensors;
        source.p0 = cart2grid(kgrid, sensors(:,i));

        [t_array, dt] = makeTime(kgrid, 1800);

        % set the input arguments
        kgrid.t_array = t_array;
        input_args = {'PlotSim',false, 'DataCast', 'double'};
        % run the simulation
        sensor_data = kspaceFirstOrder2D(kgrid, m, source, sensor, input_args{:});
        
        % calculating arrival time
        
%         [arrival_t_s, rx_idx_s] = arrival_time_f(i, sensor_data, t_array);
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%

        par_save([save_dir '/' int2str(i) '.mat'], sensor_data, t_array);
        toc;
    end
    save([save_dir, '/sensor_pos.mat'], 'sensors');

     disp('Done');
     exitcode = 0;
 
end