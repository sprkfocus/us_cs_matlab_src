function exitcode = simulation100( z_start_idx, z_end_idx )
    z_start_idx = str2num(z_start_idx);
    z_end_idx = str2num(z_end_idx);
    z_steps = 15;
    if z_start_idx < 1 || z_end_idx > z_steps || z_start_idx > z_end_idx
        error('check z bounds');
    end
    
    exitcode = 1;
    mkdir('sim_res');
    nSource = 100;
    
    
    save_dir = ['sim_res/' int2str(nSource)];
    mkdir(save_dir);

    %medium.alpha_mode = 'no_dispersion';
    disp('Starting overall simulation...');

    side_len = 6e-2; % [m]
    height_len = 2.5e-2;

    z_step_len = height_len / z_steps;
    z_top = -1e-2;
    
    Nx = 128;
    Nx_clear = Nx - 22*2;

    dx = side_len / Nx_clear;
    dy = side_len / Nx_clear;
    
    % t1  pos
    radius1 = 3.75 * 1e-3; % [m]
    c1_x = floor(-1e-2 /dx + Nx/2);
    c1_y = floor(-1e-2 /dy + Nx/2);
    c1_zp = -0.5e-2;
    % t2
    radius2 = 2.5 * 1e-3; % [m]
    c2_x = floor(1.5e-2 /dx + Nx/2);
    c2_y = floor(1.5e-2 /dy + Nx/2);
    c2_zp = 1e-2;
    
    sensor_radius = (Nx/2 - 22) * dx;
    sensors = makeCartCircle(sensor_radius, nSource);

    for z_step = z_start_idx : z_end_idx
        disp('------------------------------------------------------------');
        disp(['Z Iteration: ' int2str(z_step) ' / ' int2str(z_end_idx)]);
        tic;
        save_dir_z = [save_dir '/' int2str(z_step) ];
        mkdir(save_dir_z);
        z_pos = z_top + z_step_len * z_step;
        
        speed = zeros(Nx,Nx);
        density = zeros(Nx,Nx);
        alpha_coeff = zeros(Nx,Nx);
        BonA = zeros(Nx,Nx);


        [IH, IW] = ndgrid(1:Nx, 1:Nx);
        tumor_model = zeros(Nx,Nx); tumor_model = tumor_model > 0;
        % tumor 1 radius 
        d_centre1 = abs(z_pos - c1_zp);
        if d_centre1 < radius1
           r1_shifted = (radius1^2 - d_centre1^2)^0.5 /dx;
           d = ((IH-c1_x)./r1_shifted).^2 + ((IW-c1_y)./r1_shifted).^2;
           tumor1 = d < 1.0;
           tumor_model = tumor_model | tumor1;
        end
            
        % tumor 2 
        d_centre2 = abs(z_pos - c2_zp);
        if d_centre2 < radius2
           r2_shifted = (radius2^2 - d_centre2^2)^0.5 /dx;
           d = ((IH-c2_x)./r2_shifted).^2 + ((IW-c2_y)./r2_shifted).^2;
           tumor2 = d < 1.0;
           tumor_model = tumor_model | tumor2;
        end

        % figure, imagesc(tumor_model);

        water = ~tumor_model;
        medium = [];

        speed(water) = 1500;            % [m/s]
        alpha_coeff(water) = 0.002;     % [dB/(MHz^y cm)]
        density(water) = 1000;
        BonA(water) = 6.0;

        speed(tumor_model) = 2600;
        alpha_coeff(tumor_model) = 0.75;
        density(tumor_model) = 1700;
        BonA(tumor_model) = 8.0;


        medium.sound_speed = reshape(speed,[Nx Nx]);
        medium.sound_speed_ref = 1460;
        medium.density = reshape(density,[Nx Nx]);
        medium.alpha_coeff = reshape(alpha_coeff,[Nx Nx]);
        medium.alpha_power = 1.5;
        medium.BonA = reshape(BonA,[Nx Nx]);

        kgrid_base = makeGrid(Nx, dx, Nx, dy);

        for i = 1 : 1 
            disp(['Iteration(plane): ' int2str(i) '/' int2str(nSource) ]);
            kgrid = kgrid_base;
            m = medium;
            source = [];
            sensor = [];
            sensor.mask = sensors;
            source.p0 = cart2grid(kgrid, sensors(:,i));

            [t_array, dt] = makeTime(kgrid, 2000);

            % set the input arguments
            kgrid.t_array = t_array;
%             input_args = {'PlotSim',false};
            input_args = {'PlotLayout',false, 'PMLInside', true, 'PlotScale', [0 0.05], ...
                'RecordMovie', true, 'MovieArgs', {'compression','None'}, 'MovieType', 'image', ...
                'MovieName', [save_dir '/movie_' int2str(z_step)]};
            % run the simulation

            sensor_data = kspaceFirstOrder2D(kgrid, m, source, sensor, input_args{:});

            % calculating arrival time

%             [arrival_t_s, rx_idx_s] = arrival_time_f(i, sensor_data, t_array);

            par_save([save_dir_z '/' int2str(i) '.mat'], sensor_data, t_array);
            
        end
        disp(':Z iteration done:');
        toc;
    end
    save([save_dir, '/sensor_pos.mat'], 'sensors');

    disp('Done');
    exitcode = 0;
 
end