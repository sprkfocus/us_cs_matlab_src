%  processing big data

% Main script for the evaluation

speed_model = simulated_model_bigdata();

load_clear = false;
do_default_reconstruction = false;
if load_clear
    load('temp_result.mat');
elseif do_default_reconstruction
    t_s = cputime();
    slice_clear = reconstruct_clear(3.66756e-09, 8.15307e-07, 1, [], [], true); 
    clear_elapsed = cputime() - t_s;
    save('temp_result.mat', 'slice_clear', 'clear_elapsed');
else
   slice_clear = eye(64); 
end

% cs_s = cs_analysis(slice_clear, false);
cs_s = 13^2;
cs_N = size(speed_model, 1)^2;
cs_m = 4 * cs_s * log(cs_N / cs_s);
t_s = cputime();
slice_cs = reconstruct_cs_classic(round(cs_m), true);
cs_elapsed = cputime() - t_s;

disp(['Regular time for reconstruction: ' num2str(+Inf) ' [s]']);
disp(['CS time for reconstruction:      ' num2str(cs_elapsed) ' [s]']);

figure;
subplot(131);
[img_ref, x_arg, y_arg] = make_picture(speed_model);
imagesc(x_arg *1000, y_arg *1000, img_ref);
xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('Model');
hold on;

subplot(132);
[img_basic, x_arg, y_arg] = make_picture(slice_clear);
imagesc(x_arg *1000, y_arg *1000, img_basic);
xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('Original reconstruction algorithm');

subplot(133);
[img_cs, x_arg, y_arg] = make_picture(slice_cs);
imagesc(x_arg *1000, y_arg *1000, img_cs);
xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('CS reconstruction algorithm');
hold off;