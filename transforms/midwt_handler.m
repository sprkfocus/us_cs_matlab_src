function [ f ] = midwt_handler( x, wav )
    try
        f = midwt(x, wav);
    catch
        warning('midwt: dealing with error');
        f = midwt( mdwt(x * 0, wav), wav);
    end


end

