function [ x ] = mdwt_handler( f, wav )
    try
        x = mdwt(f, wav);
    catch
       warning('dealing with errors');
       x = mdwt(f*0, wav);
    end

end

