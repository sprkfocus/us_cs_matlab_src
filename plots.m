% plots maker
set(0,'DefaultAxesFontSize',22)
N = 64;

%--------------------------------------------------------------------------
% SNR

plot(tx_custom, snr_error(tx_custom, z_i), 'LineWidth', 3);
xlabel('Used projections');
ylabel('SNR');


plot(percents, norm_fro_kwave, 'LineWidth', 3); 
xlabel('Amount of the data in use [%]'); 
ylabel('SNR');
xlim([percents(1) percents(18)]);

return;

%--------------------------------------------------------------------------
% model 3d
z_count = 15;
load('X_model.mat');
Xvis = X_model;
for z = 1 : z_count
   Xvis(:,:,z) = speedmap2grayscale(Xvis(:,:,z), false); 
end

figure;
hold on;
[ax, ay, az] = meshgrid(1:N,1:N,1:z_count);
for a = 10.^(1:4) % 'a' defines the isosurface limits
    p = patch(isosurface(ax,ay,az,Xvis,max(Xvis(:))/a)); % isosurfaces at max(V)/a
    isonormals(ax,ay, az, Xvis, p); % plot the surfaces
    set(p,'FaceColor','red','EdgeColor','b'); % set colors
end
alpha(.3); % set the transparency for the isosurfaces
daspect([1 1 1])
view(3), axis vis3d , box on, grid on
camproj perspective
camlight, lighting phong, alpha(.5)




return;

X = volF(:,:,3);
F = speedmap2grayscale(X);

imagesc(F, [0 255]);
colormap(gray(256));


title('Image, 100% sensors in use');

xlabel('X [mm]');
ylabel('Y [mm]');
% axis('image');



return;
plot(tx_custom, error(tx_custom, 3), 'LineWidth', 3);

title('Norm between model and reconstructed image');
xlabel('Sensors in use');
ylabel('Frobenius norm');


return;

k = [64 128 256 512 1024 2048];
N = k .^2;

mf = @(N) 4*13^2*log(N/144);

m = arrayfun(mf, N);

percents = m ./ N;

plot(k, percents * 100, 'LineWidth', 3);

xlabel('Sensors number');
ylabel('m / M ratio [%]');





return;

plot(arrayfun(@(x) x^2, 1:25), norm_len, 'LineWidth', 3);
title('Norm between source and decompressed image');
xlabel('Coefficients in use');
ylabel('Frobenius norm')

xlim([0 26^2]);



return;

figure;
mesh(X);
title('Speed map in frequency domain');

xlabel('X [mm]');
ylabel('Y [mm]');
zlabel('Amplitude');

axis tight;


return;
load('X_full.mat');

X = volF(:,:,3);
mesh(X);
title('Speed map');

xlabel('X [mm]');
ylabel('Y [mm]');
% axis('image');

return;

Xg = speedmap2grayscale(X);

imagesc(Xg, [0 255]);
colormap(gray(256));
title('Image of phantom');
xlabel('X [mm]');
ylabel('Y [mm]');

return;

load('sim_res/100/arrivals_z3_33s');

plot(1:33, arrival_t(2409:2409+32), 'LineWidth', 3);

title('Arrival time');

xlabel('Sensor id');
ylabel('Time [s]');
axis tight;


return;
load('/home/esenin/Projects/Ultrasonic/software/reconstruction codes/sim_res/100/3/1.mat');
figure;

plot(t_array, sensor_data(60, :), 'LineWidth', 3);

title('Acoustic pressure');
xlabel('Time [s]');
ylabel('Pressure [Pa]');

axis tight;



return
imagesc(1:100, t_array,abs(sensor_data'), [0 0.03]);imagesc(1:100, t_array,abs(sensor_data'), [0 0.03]);


set(0,'DefaultAxesFontSize',22)

title('Acoustic pressure in time-domain [Pa]', 'FontSize', 22);



xlabel('Sensor id', 'FontSize', 22);
ylabel('Time [s]', 'FontSize', 22);
c = colorbar;
xlabel(c, 'Pressure [Pa]','FontSize', 22 ); 
colorbar;


