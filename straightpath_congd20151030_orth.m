% function straightpath_congd20151021_3
clear all;
clc
m=65; n=65;                      %m 行结点数，n 列结点数
Length=4e-2;Width=4e-2;          %Length:x方向的长度，Width:y方向的长度
d_x=Length/(n-1);d_y=Width/(m-1);%d_x,d_y为 x,y方向上的每一格的长度
M=(m-1)*(n-1);                   %成像区域分成M个网格
c=1500;                          %水的速度
s0=1/c;
load('E:\matlab\shortestpath\newshort_exper\bentray\pos_receiver_625.mat');    %阵元的结点位置
b0=size(Bt1,2);
N2=1/18*b0; N3=2*N2+1;           %N3为成像时用到的接收阵元数即接收孔径大小，发射阵元对面的接收阵元一侧是N2个阵元

load ('E:\matlab\shortestpath\newshort_exper\bentray\str_9_t_65_AIC_filter_tr_re_pair_90.mat'); %输入接收孔径中阵元的渡越时间向量T0（N3*b0维的列向量）
load ('E:\matlab\shortestpath\newshort_exper\bentray\str_9_path_65_effective.mat');%从发射阵元到接收孔径中阵元的路径矩阵L_3（N3*b0行，M列）
% wk='db1';            % 正交小波基的类型 D2 小波(此时是Haar变换)
% N=M;                 % 正交小波基的长度
% psi=DWTself(N,wk);   %正交小波变换矩阵
N=size(L_3_effective,2);
SS0=s0.*ones(N,1);       %初始的慢度向量
psi=orth(rand(N,N));
Tol_grad=1e-16;       %梯度误差精度

outmaxiter=100;      %共轭梯度的最大迭代次数
% innermaxiter=100;  %线性搜索的最大次数
lamda=1e-10;          %稀疏性正则化参数
lamda_tv=1e-7;          %全变分正则化参数
sigma=zeros(N);      %预定义对角阵
z=1;                 %while的判断条件中z的初始值
alpha=0.01;beta=0.5;  %0<alpha<0.5;0<beta<1;%backline search
t_=5;                %backline search中t_的初始值
n0=0;                %迭代次数的初始值
% n_inner=0;            %线性搜索的迭代次数



while (z)
  n0  
SS(:,n0+1)=SS0;      %记录每次的慢度向量
pa=psi'*SS(:,n0+1);  %正则项中向量
%下面循环用来定义一个对角阵
   for ii=1:N
       for jj=1:N
           if jj==ii
            sigma(ii,jj)=sqrt((pa(ii))^2+eps);
           end
       end
   end
G0=L_3_effective;              %raytrace matrix 72*9*M维，每次迭代要更新
C0(:,n0+1)=norm((G0*SS(:,n0+1)-T0),2)^2+lamda*norm(pa,1)+lamda_tv*f_1norm_tv(SS(:,n0+1));  %目标函数
C0_1(:,n0+1)=norm((G0*SS(:,n0+1)-T0),2)^2;  %记录C0_1,C0_2
C0_2(:,n0+1)=norm(pa,1);
C0_3(:,n0+1)=f_1norm_tv(SS(:,n0+1));
% lamda=C0_1/C0_2;                    %每次迭代更新lamda
r0=G0*SS(:,n0+1)-T0;                  
grad_C0(:,n0+1)=2*G0'*r0+lamda*psi*1/(sigma)*pa+lamda_tv*grad_1norm_tv(SS(:,n0+1));    %目标函数的梯度
grad_C1=grad_C0(:,n0+1);              %grad_C1存放每次的梯度


    if  n0==0
        gamma_1=0;                                  %gamma_1共轭方向中的参数
        delta_m_1=-grad_C0(:,n0+1);                 %delta_m_1共轭方向
       
    else
        yta(:,(n0+1))=grad_C1-grad_C0(:,n0);
        gamma_1=max(0,min((grad_C1'*yta(:,(n0+1)))/(delta_m_0'*yta(:,(n0+1))),norm(grad_C1,2)^2/(delta_m_0'*yta(:,(n0+1)))));
        delta_m_1=-grad_C1+gamma_1*delta_m_0;
    end
        
       %用backline search 寻找步长u_n
       while (norm((G0*(SS(:,n0+1)+t_*delta_m_1)-T0),2)^2+lamda*norm(pa,1)+lamda_tv*f_1norm_tv(SS(:,n0+1)))>...
               (norm((G0*SS(:,n0+1)-T0),2)^2+lamda*norm(pa,1)+lamda_tv*f_1norm_tv(SS(:,n0+1))+alpha*t_*real(grad_C1'*delta_m_1))
           t_=beta*t_;
%            n_inner=n_inner+1;
       end

           u_n0=t_;
           SS1=SS0+u_n0*delta_m_1;                  %更新慢度向量
            f=norm(u_n0*delta_m_1,2)/norm(SS0,2);   
%              f=norm(grad_C1,2);
           if (f>Tol_grad)&&(n0<outmaxiter)
               z=1;
           else
               z=0;
           end
        %更新变量
        delta_m_0=delta_m_1;
        SS0=SS1;
        n0=n0+1;
        t_=5;
%         lamda=lamda*0.85;                           %每次迭代更新lamda 
end

SS_effective=SS1;
load('E:\matlab\shortestpath\newshort_exper\bentray\SS_res.mat');              
num_SS_res=numel(SS_res);
SS_final=Inf*ones(M,1);
for jj=1:num_SS_res
   SS_final(SS_res(jj))=SS_effective(jj);
end



     
   VDOTMN=zeros(m,n);  %速度矩阵    

    %将S对应到VDOTMN    
  for k=1:M
   I=floor(k/(n-1));J=mod(k,(n-1));
   if (I~=0)&&(J~=0)
  VDOTMN(I:I+1,J:J+1)=1/SS_final(k);
   elseif(I==0)&&(J~=0)
  VDOTMN(1:2,J:J+1)=1/SS_final(k);    
   elseif(I~=0)&&(J==0)
  VDOTMN(I:I+1,n-1:n)=1/SS_final(k);

   end
  end
           
          
% make logarithmic compression to a 60 dB dynamic range（无对数压缩无动态范围）
% with proper units on the axis
env_dB1=(VDOTMN-min(min(VDOTMN)))/(max(max(VDOTMN))-min(min(VDOTMN)));
% env_dB1=(VDOTMN/max(max(VDOTMN)));
% env_dB2=env_dB1-max(max(env_dB1));
env_gray=255*env_dB1;

depth=(0:size(VDOTMN,1)-1)*d_y;
x=(0:size(VDOTMN,2)-1)*d_x;
image(x*1000, depth*1000, env_gray)
% image(x*1000, depth*1000, VDOTMN)

xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('Image of breast phantom')
           
