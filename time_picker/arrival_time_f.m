function [arrival_t, rx_indexs] = arrival_time_f(tx_id, sensor_data, t_array, t_shift, r_threshold)
    if size(sensor_data, 1) == 1024
        r_threshold = 2e-4;       
        t_shift = 1.04e-6;
    end
    tx_num = 1;
    rx_num = 1024;
    use_receivers = 129;  % 72{9 / 43 / 63} / 923 | 100 {65, 33, 15, 9}
    
    
    data_dec = rx_num - use_receivers;
    near_idx = round(data_dec/2);


    arrival_t = zeros(rx_num - data_dec, 1);  
    rx_indexs = zeros(1, rx_num - data_dec);  

    ariv_idx = 1;

    a0 = sensor_data';
    t0 = t_array;
%     record_len = max(size(a0));
    
    rx_counter = 1;
    
    for rx_id = 1 : rx_num
        if  abs(tx_id - rx_id) < near_idx || abs(min(tx_id, rx_id) + rx_num - max(tx_id, rx_id)) < near_idx
            continue
        end
        rx_indexs(1, rx_counter) = rx_id;
        rx_counter = rx_counter + 1;
        
        x = a0(:, rx_id);
        
        no_signal_zone = 100;
        base_amp = max(abs(x(1:no_signal_zone)));

        
        for i = no_signal_zone : size(x)
           if abs(x(i)) - base_amp > r_threshold
               idx = i;
               break;
           end
        end
        
        
        arrival_t(ariv_idx) = t0(idx) + t_shift;
        ariv_idx = ariv_idx + 1;
%         disp(rx_id);
%           t0(idx)
%           plot(t0, x);
%           figure
%           subplot(2,1,1);
%           plot(t0, x);
%           subplot(2,1,2);
%           plot(t0, er_x);  
    end
    
end

