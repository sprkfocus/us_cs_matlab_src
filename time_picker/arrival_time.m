% compose 72*71 arrival time vector
function [arrival_t, rx_indexs] = arrival_time(z_start, z_count, do_save, t_shift, r_threshold)
    if nargin == 4
        r_threshold = 1e-4;
    elseif nargin ~= 5
        error('check args');
    end

    start_tx = 1;
    tx_num = 1024;

    use_big_sim = true;

    use_china_data = false;

    if use_china_data
        save_path = 'data/datamat/';
    elseif use_big_sim 
        save_path = 'sim_res/1024/';
    else 
        save_path = 'sim_res/100/';
    end

    data_dec = 895; %75;
    arrival_t = zeros(tx_num * (tx_num - data_dec), 1);  
    rx_indexs = zeros(tx_num, tx_num - data_dec);  

    for z = z_start : z_count
        ariv_idx = 1;
        if z_count > 0
            data_src_dir = [save_path int2str(z) '/'];
        else
            data_src_dir = save_path;
        end

        tic;
        for tx_id = start_tx : tx_num
            f_name = [data_src_dir int2str(tx_id) '.mat']
            load(f_name); 

            
            [arriv_s, rx_id_s] = arrival_time_f(tx_id, sensor_data, t_array, t_shift, r_threshold);

            for i = 1 : size(arriv_s, 1)
               arrival_t(ariv_idx) = arriv_s(i);
               ariv_idx = ariv_idx + 1;
               rx_indexs(tx_id, i) = rx_id_s(i);
            end



        end
        if do_save
            toc;
            if use_china_data
                ending = 'c.mat';
            else
                ending = 's.mat';
            end
            save_file = [save_path 'arrivals_z' int2str(z) '_' int2str(size(rx_id_s, 2)) ending]
            save(save_file, 'arrival_t', 'rx_indexs');
        end

    end
end
