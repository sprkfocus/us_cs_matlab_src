function [slice, cost_f_seq, rmse_seq] = reconstruct_4096( ...
        do_bigdata, cs_A, ...
        do_use_full_bentray, ...
        lamda, lamda_tv, ...
        do_use_temp_data ...
        )
% function straightpath_congd20151021_3
    include_subpaths();
    t_step_init = 5;
    if exist('do_use_temp_dir') && do_use_temp_data
        data_prefix = 'temp_';
    else
        data_prefix = '';
    end
    if ~exist('lamda', 'var') || (lamda == 0)
        lamda= 8e-9;  %3.66756e-09;          
        lamda_tv=8.15307e-07;  
    end
    if exist('do_bigdata', 'var') && do_bigdata
        data_dir = ['sim_results/1024/'];
        Length= 20e-2;    
        Width=  20e-2; 
    else
        data_dir = ['sim_results/wave_s1/'];
        Length= 4e-2;    
        Width=  4e-2;    
        do_bigdata = false;
        speed_model = simulated_model_1();
    end
    if ~exist('use_full_bentray', 'var')
        do_use_full_bentray = false;
    end
    if exist('cs_A','var') && max(size(cs_A)) == 0
        clearvars cs_A;
    end
    
    outmaxiter = 43;
    m=65; n=65;                     
          
%     d_x=Length/(n-1);d_y=Width/(m-1);

    
    load([data_dir 'sensor_pos.mat']);
    sensors_pos = sensors_pos_abs2grid(sensors, Length, Width, m, n);
    
    
    load([data_dir data_prefix 'arrivals_t.mat']);
    rx_indexs_adv = rx_index_input2advanced(rx_indexs);
    full_equations_amout = size(rx_indexs, 1) * size(rx_indexs, 2);
    
 
    if exist('cs_A', 'var') && max(size(cs_A)) > 0
       arrival_t = cs_A * arrival_t; 
    end

    speed_init = 1500; % m/s
    slowness_init=1/speed_init;
     

    DecLevel = 1;
    dbcoef = 4;
    W = Wavelet('Daubechies',dbcoef,DecLevel);	% Wavelet transform

    N_side = 64;
    N = N_side^2; %  effective cells
    psi_T = @(x) reshape( W * reshape(x, [N_side N_side]), [N 1] );
    psi_ = @(x) reshape( W'* reshape(x, [N_side N_side]), [N 1] );
    
    SS0=slowness_init.*ones(N,1);


    init_raylenth_file = ['cached/G_init_' int2str(full_equations_amout)  '_' int2str(N)  '.mat'];
    load_paths = 2 == exist(init_raylenth_file, 'file');


    
    load('helpers/SS_res.mat'); 
    
    M = (m - 1)*(n - 1);
    
    Tol_grad=1e-16;    

    sigma=zeros(N);      
    iter_cond=1;                
    alpha=0.01;beta=0.5;   
    t_=t_step_init;                
    n0=0;     
    cost_f_seq = zeros(outmaxiter, 1);
    rmse_seq = zeros(outmaxiter, 1);

    
    calc_cost_f = @(G, x)   norm((G * x - arrival_t), 2)^2 ...
                            +lamda* norm(psi_T(x), 1) ;%...
%                              +lamda_tv* f_1norm_tv(x);
                        
                
    
    while (iter_cond)
        if mod(n0, 10) == 0
            disp(['reconstruct iter: ' int2str(n0) ' / ' int2str(outmaxiter) ]);  
        end
        SS_cur = SS0;      
        psiT_slow = psi_T( SS_cur); 

        for ii=1:N
            sigma(ii,ii)=sqrt((psiT_slow(ii))^2+eps);
        end
        
        if (n0 == 0 && load_paths)
            load(init_raylenth_file);
            if size(G0, 1) ~= full_equations_amout || size(G0, 2) ~= N
                clearvars G0;
            elseif exist('cs_A', 'var')
                G0 = cs_A  * G0;
            end
        end
        if ( (n0 == 0 && ~exist('G0', 'var')) || (n0 > 0 && do_use_full_bentray) )
%             warning('Broken concept. TODO?');
            disp('start raylength matrix calculating...'); 
            calc_start = cputime();
            slowness_map = reshape(applyMask(SS_cur, slowness_init), [N_side N_side]);   
            dx = Length / (m - 2);
            G0 = calc_Raylength_tb_eval(slowness_map, sensors_pos, rx_indexs_adv, dx, dx);
          
            disp([' > raylength matrix is ready! Cputime elapsed: ' num2str(cputime() - calc_start) '[s]']);
            if n0 == 0 && ~load_paths
                save(init_raylenth_file, 'G0'); disp('G0[0] has been saved'); 
            end
            if exist('cs_A', 'var')
                G0 = cs_A  * G0;
            end
        end
        
        if n0 == 0
            G_prev = G0;
        end
        
        
%         C0(:,n0+1)=norm((G0*SS(:,n0+1)-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS(:,n0+1));  %Ŀ�꺯��
        r0=G_prev * SS_cur-arrival_t;                  
        grad_C0=  2* G0' * r0 ...
            + lamda* psi_(sigma\psiT_slow) ;%...
%             + lamda_tv/10*grad_1norm_tv(SS_cur);    

        grad_C0 = applyMask(grad_C0, 0);
        if  n0==0
            gamma_1=0;                                  %gamma_1������еĲ���
            delta_m_1=-grad_C0;                 %delta_m_1�����

        else
%             yta = grad_C0-grad_C0_prev;
%             gamma_1=max(0,min((grad_C0'*yta)/(delta_m_0'*yta),norm(grad_C0,2)^2/(delta_m_0'*yta)));
%             delta_m_1=-grad_C0+gamma_1*delta_m_0;
            ytn =  norm(grad_C0 ,2 )^2 / norm(grad_C0_prev, 2)^2;
            delta_m_1 = -grad_C0 + ytn * delta_m_0;
        end
        
        delta_m_1 = applyMask(delta_m_1, 0);

        
        cost_value_cur = calc_cost_f(G0, SS_cur);
        while calc_cost_f(G0, SS_cur + t_ * delta_m_1) > ...
               cost_value_cur + alpha* t_* real(grad_C0' * delta_m_1) 
           t_=beta*t_;
        end
        

        u_n0=t_;
        if u_n0 < 0.1
           disp(['mu =' num2str(u_n0)]) 
        end
        SS1=SS0+u_n0*delta_m_1;                 
        f=norm(u_n0*delta_m_1,2)/norm(SS0,2);   

        if (f>Tol_grad)&&(n0<outmaxiter)
           iter_cond=1;
        else
           iter_cond=0;
        end

        G_prev = G0;
        grad_C0_prev = grad_C0;
        delta_m_0=delta_m_1;
        SS0=SS1;
        n0=n0+1;
        t_=t_step_init;

         % making report
        cost_f_seq(n0) = calc_cost_f(G0, SS1);
        rmse_seq(n0) = RMSE(1./reshape(SS1, [N_side N_side]), speed_model);
    end

    SS_effective=extract_effective(SS1, 'zip_ss');

    num_SS_res=numel(SS_res);
    SS_final=slowness_init*ones(M,1);
    for jj=1:num_SS_res
       SS_final(SS_res(jj))=SS_effective(jj);
    end

     
   VDOTMN=zeros(m,n);  
   
    for k=1:M
        I=floor(k/(n-1));
        J=mod(k,(n-1));
        if (I~=0)&&(J~=0)
            VDOTMN(I:I+1,J:J+1)=1/SS_final(k);
        elseif(I==0)&&(J~=0)
            VDOTMN(1:2,J:J+1)=1/SS_final(k);    
        elseif(I~=0)&&(J==0)
            VDOTMN(I:I+1,n-1:n)=1/SS_final(k);
        end
    end
           
    slice = VDOTMN(1:64,1:64);
end


          
          
