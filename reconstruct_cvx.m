function [slice] = reconstruct_cvx( do_bigdata, cs_A, ...
                                    do_use_full_bentray, ...
                                    lamda, lamda_tv, t_step_init, ...
                                    do_use_temp_data)
% function straightpath_congd20151021_3
    include_subpaths();
    if exist('do_use_temp_dir') && do_use_temp_data
        data_prefix = 'temp_';
    else
        data_prefix = '';
    end
    if ~exist('lamda', 'var') || (lamda == 0)
        lamda= 3.66756e-09;          
        lamda_tv=8.15307e-07;  
        t_step_init = 1;
    end
    if exist('do_bigdata', 'var') && do_bigdata
        data_dir = ['sim_results/1024/'];
        Length= 20e-2;    
        Width=  20e-2; 
    else
        data_dir = ['sim_results/wave_s1/'];
        Length= 4e-2;    
        Width=  4e-2;    
        do_bigdata = false;
    end
    if ~exist('use_full_bentray', 'var')
        do_use_full_bentray = true;
    end

    
    outmaxiter = 15;
    m=65; n=65;                     
          
%     d_x=Length/(n-1);d_y=Width/(m-1);

    
    load([data_dir 'sensor_pos.mat']);
    sensors_pos = sensors_pos_abs2grid(sensors, Length, Width, m, n);
    
    
    load([data_dir data_prefix 'arrivals_t.mat']);
    rx_indexs_adv = rx_index_input2advanced(rx_indexs);
    full_equations_amout = size(rx_indexs, 1) * size(rx_indexs, 2);
    
 
    if exist('cs_A', 'var') && max(size(cs_A)) > 0
       arrival_t = cs_A * arrival_t; 
    end

    speed_init = 1500; % m/s
    slowness_init=1/speed_init;
    

    N=3196; %  effective cells
    SS0=slowness_init.*ones(N,1);  

    psi_filename = ['cached/psi_' int2str(N) '.mat'];
    if exist(psi_filename, 'file')
        load(psi_filename); 
    else
        psi_=orth(rand(3196, 3196));
        save(psi_filename, 'psi_');
    end
    
    init_raylenth_file = ['cached/G_init_' int2str(full_equations_amout)  '_' int2str(N)  '.mat'];
    load_paths = 2 == exist(init_raylenth_file, 'file');


    
    load('helpers/SS_res.mat'); 
    
    M = (m - 1)*(n - 1);
    
    Tol_grad=1e-16;    


    load(init_raylenth_file);
    if size(G0, 1) ~= full_equations_amout
        error('Check raylength matrix')
    end
            
    gamma_max = norm(G0'* arrival_t, 'inf');
    gamma = 0.1*gamma_max;

    MAX_ITER = 70;
    ABSTOL   = 1e-4;
    RELTOL   = 1e-2;   
    
    t_st = cputime();

    cvx_begin quiet
        cvx_precision low
        variable x(N)
        minimize(sum_square(G0*x - arrival_t) + lamda*norm(psi_' * x, 1) + lamda_tv * f_1norm_tv_cvx(x))
    cvx_end

    h.x_cvx = x;
    h.p_cvx = cvx_optval;
    h.cvx_toc = cputime() - t_st;
    disp('Done.!');
    
    
    
    

    SS_effective=x;

    num_SS_res=numel(SS_res);
    SS_final=slowness_init*ones(M,1);
    for jj=1:num_SS_res
       SS_final(SS_res(jj))=SS_effective(jj);
    end

     
   VDOTMN=zeros(m,n);  
   
    for k=1:M
        I=floor(k/(n-1));
        J=mod(k,(n-1));
        if (I~=0)&&(J~=0)
            VDOTMN(I:I+1,J:J+1)=1/SS_final(k);
        elseif(I==0)&&(J~=0)
            VDOTMN(1:2,J:J+1)=1/SS_final(k);    
        elseif(I~=0)&&(J==0)
            VDOTMN(I:I+1,n-1:n)=1/SS_final(k);
        end
    end
           
    slice = VDOTMN(1:64,1:64);
end


          
          
