function TT=f_1norm_tv(x)
    if min(size(x)) == 1 && max(size(x)) == 3196
       x = extract_effective(x, 'unzip_ss'); 
    end
    x=[x(:,1) x x(:,end)];
    x=[x(1,:);x;x(end,:)];
    df_x=(x(2:end-1,3:end)-x(2:end-1,1:end-2))/2;
    df_y=(x(3:end,2:end-1)-x(1:end-2,2:end-1))/2;
    TT=sum(sum(sqrt(df_x.^2+df_y.^2)));
    
end