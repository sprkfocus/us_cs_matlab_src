function [ t_sifted, rx_i_sifted, use_idx ] = sift_data( arrival_t, rx_indexs, in_use, full_randomize )
    if ~full_randomize
        rng('default'); rng(2016);
    end
        
    tx_num = size(rx_indexs, 1);
    if tx_num == in_use
       t_sifted = arrival_t;
       rx_i_sifted = rx_indexs;
       use_idx = 1:in_use;
       return;       
    end
    
    
    perm_idx = randperm(tx_num);
    use_idx = perm_idx(1 : in_use);
    
    
    
    rx_num = size(rx_indexs, 2);
    
    t_sifted = zeros(in_use * rx_num, 1);
    rx_i_sifted = zeros(in_use, rx_num);
    
    for i = 1 : in_use
        ariv_start = (use_idx(i) - 1) * rx_num + 1;
        ariv_end = use_idx(i) * rx_num;
        t_sifted( rx_num * (i-1) + 1 : rx_num * i , 1) = arrival_t(ariv_start : ariv_end, 1);
        rx_i_sifted (i, :) = rx_indexs(use_idx(i), :);        
    end
    

end

