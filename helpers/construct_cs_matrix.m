function [ cs_A, used_cols ] = construct_cs_matrix( cs_m, cs_M, mrand_type, val_type, rx_num)
% [ cs_A, used_proj ] = construct_cs_matrix( cs_m, cs_M, mrand_type)
% mrand_type: 'use_all', 'use_iid_proj', 'no_line_comb', 'save_tx'
% val_type: 'ones', 'cs'
    mrand_types = {'all', 'iid', 'no_line_comb', 'save_tx'};
    val_types = {'ones', 'cs'};
    
    validatestring(mrand_type, mrand_types);

    validatestring(val_type, val_types);


    rng('default'); rng(2016);

    if streq(val_type, 'ones')
        fill_val = 1;
    else
        fill_val =  sqrt(3 / cs_m);
    end

    disp('Construct cs matrix...');
    
    rand_idxs = randperm(cs_M);
     

    if streq(mrand_type, 'no_line_comb')
        cs_A = zeros(cs_m, cs_M);
        % fill A matrix
        for i = 1 : cs_m
            line_idx = rand_idxs(i);
            cs_A(i, line_idx) = 1;        
        end
        used_cols = cs_m;
        return;
    end

    cs_A = randi(6, cs_m, cs_M);
    for i = 1 : cs_m
        for j = 1 : cs_M 
            if cs_A(i, j) == 1
                cs_A(i, j) = fill_val;
            elseif cs_A(i, j) == 6
                cs_A(i, j) = -fill_val;
            else
                cs_A(i, j) = 0;
            end                
        end
    end
       
    if streq(mrand_type, 'iid')
        for i = 1 : cs_M - cs_m
            extra_id = rand_idxs(i);
            cs_A(:, extra_id) = 0;
        end
    end
    if streq(mrand_type, 'save_tx')
        tx_num = floor(cs_M / rx_num);
        need_proj = ceil( cs_m / rx_num );
        tx_idxs_rm = randperm(tx_num);
        tx_idxs_rm = tx_idxs_rm(1 : tx_num - need_proj);
        
        for tx_rm = tx_idxs_rm
           line_start = (tx_rm - 1) * rx_num + 1;
           line_end = tx_rm * rx_num;
           cs_A(:, line_start : line_end) = 0;            
        end
        
        
    end
    
    used_cols = 0;
    for i = 1 : cs_M
       if sum(abs(cs_A(:, i))) > 0
          used_cols = used_cols + 1;
       end
    end
    
    disp('Done.')
    
end

