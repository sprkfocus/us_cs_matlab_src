function [ bool ] = streq(lhs, rhs)

    bool = strcmp(lhs, rhs);

end

