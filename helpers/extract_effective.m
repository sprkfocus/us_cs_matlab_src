function [ X ] = extract_effective( F, act_type )
% [ X ] = extract_effective( F, act_type )
% act_type: zip_ss, unzip_ss, zip_psi, unzip_psi

    if nargin < 2
        act_type = 'zip_ss';
    end
    slow_init = 1/1500;
    N_effective = 3196;
    N = 64;

       
    filename = 'mask.mat';
    if exist(filename, 'file') == 0
        error('need mask') ;
    else
        load(filename);
    end
    
    if streq(act_type, 'unzip_ss')

        X = slow_init * ones(N^2, 1);
        cnt = 1;
        for i = 1 : size(mask, 1)
           if mask(i) == 1
               X(i) = F(cnt);
               cnt = cnt + 1;
           end
        end
        if cnt - 1  ~= max(size(F))
            error('compression mask doesnt fit to your signal');
        end
        
        X = reshape(X, [N N]);
    elseif streq(act_type, 'zip_ss')
        maskInf = maskInf .^(-1);
        N_ = sum(maskInf(:));
        X = zeros(N_, 1);
        F = F(:);
        line = 1;
        for i = 1:size(F,1)
            if maskInf(i) == 1 
                X(line) = F(i);
                line = line + 1;
            end
        end
    elseif streq(act_type, 'zip_psi')
        F = reshape(F, [N^2 1]);
        X = F(1 : N_effective);
    elseif streq(act_type,'unzip_psi')
        X = zeros(N^2, 1);
        X(1:N_effective) = F;
        X = reshape(X, [N N]);
    else
        error('Invalid action type');
    end
    
end

