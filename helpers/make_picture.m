function [ env_gray, x, depth ] = make_picture( VDOTMN )
          
    m=65; n=65;                      %m �н����n �н����
    Length=4e-2;
    Width=4e-2;          %Length:x����ĳ��ȣ�Width:y����ĳ���
    d_x=Length/(n-1);d_y=Width/(m-1);%d_x,d_yΪ x,y�����ϵ�ÿһ��ĳ���
    
    % make logarithmic compression to a 60 dB dynamic range���޶���ѹ���޶�̬��Χ��
    % with proper units on the axis
%     VDOTMN = log10(VDOTMN);
    env_dB1=(VDOTMN-min(min(VDOTMN)))/(max(max(VDOTMN))-min(min(VDOTMN)));
    % env_dB1=(VDOTMN/max(max(VDOTMN)));
    % env_dB2=env_dB1-max(max(env_dB1));
    env_gray=255*env_dB1;

    depth=(0:size(VDOTMN,1)-1)*d_y;
    
    x=(0:size(VDOTMN,2)-1)*d_x;
    



end

