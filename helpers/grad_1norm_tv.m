%  总变差的梯度----此处程序参考文献【1】
function TT=grad_1norm_tv(Solution)

epsx=1e-14;  %  防止梯度无限大

Solution=[Solution(:,1) Solution Solution(:,end)];
Solution=[Solution(1,:);Solution;Solution(end,:)];

%  自身导数
xx_1=Solution(2:end-1,2:end-1)-Solution(2:end-1,3:end);
yy_1=Solution(2:end-1,2:end-1)-Solution(3:end,2:end-1);
%  左边导数
xx_2=Solution(2:end-1,1:end-2)-Solution(2:end-1,2:end-1);
yy_2=Solution(2:end-1,1:end-2)-Solution(3:end,1:end-2);
%  上边导数
xx_3=Solution(1:end-2,2:end-1)-Solution(1:end-2,3:end);
yy_3=Solution(1:end-2,2:end-1)-Solution(2:end-1,2:end-1);

%  梯度
grad_1=sqrt(xx_1.^2+yy_1.^2+epsx);
grad_2=sqrt(xx_2.^2+yy_2.^2+epsx);
grad_3=sqrt(xx_3.^2+yy_3.^2+epsx);
            
%  总变差的梯度
TT=(xx_1./grad_1+yy_1./grad_1-xx_2./grad_2-yy_3./grad_3);