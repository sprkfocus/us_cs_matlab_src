function [ sensors_pos ] = sensors_pos_abs2grid( sensors_pos, Length, Width, m, n )
    d_x=Length/(n-1);d_y=Width/(m-1);
    sensors_pos(1,:) = (sensors_pos(1,:) + Length/2) / d_x;
    sensors_pos(2,:) = (sensors_pos(2,:) + Width/2) / d_y;
    s_min = min(sensors_pos(:)); s_max = max(sensors_pos(:));
    scale_f = @(x) (x - s_min) / (s_max - s_min) * ((m - 1) - 1) + 1;
    sensors_pos = arrayfun(scale_f, sensors_pos);    

end

