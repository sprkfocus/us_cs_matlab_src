function [ rx_index_cs_adv ] = rx_index_full2sifted_adv( rx_indexs, equations_in_use)
%create rx_indexs_adv by sifting the rx_indexs using equations indecies
    % equations in use should has cs_m size
    cs_m = max(size(equations_in_use));
    tx_num = size(rx_indexs, 1);
    rx_num = size(rx_indexs, 2);
    
    rx_indexs = rx_indexs';  
    rx_indexs = rx_indexs(:);
    used_data_ratio = max(size(equations_in_use)) / (tx_num * rx_num);
    rx_index_cs_adv = zeros(round(used_data_ratio * tx_num), round(used_data_ratio * rx_num));

    tx_id_prev = 0;
    row_id = 0;
    col_id = 2;
    for i = 1 : cs_m
        line_idx = equations_in_use(i);
        tx_id = floor(line_idx / rx_num) + 1;
        if mod(line_idx, rx_num) == 0
            tx_id = tx_id - 1;
        end
        rx_id = rx_indexs(line_idx);
        
        if tx_id ~= tx_id_prev
            row_id = row_id + 1;
            col_id = 2;
            rx_index_cs_adv(row_id, 1) = tx_id;
            tx_id_prev = tx_id;
        end
      
        rx_index_cs_adv(row_id, col_id) = rx_id;
        col_id = col_id + 1;
    end

end

