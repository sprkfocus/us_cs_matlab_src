function [ rx_indexs_adv ] = rx_index_input2advanced( rx_indexs )
%--------------  transform to new rx_index type:
   %  rx_indexs_adv = [tx_num, rx_list]
   %  every row:   [tx_id; rx_id1, rx_id2, ..., 0]
   tx_num = size(rx_indexs, 1);
   rx_num = size(rx_indexs, 2);

   rx_indexs_adv = zeros(tx_num, rx_num + 1);
   for tx_id = 1 : tx_num
      rx_indexs_adv(tx_id, 1) = tx_id;
      for rx_i = 1 : rx_num
         rx_indexs_adv(tx_id, rx_i + 1) = rx_indexs(tx_id, rx_i); 
      end
   end

end

