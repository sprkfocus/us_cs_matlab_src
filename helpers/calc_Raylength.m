function [ G ] = calc_Raylength( slowness, s_pos, rx_indexs, dx, dy )
%calc_Raylength ( slowness, s_pos, rx_indexs )
%   Detailed explanation goes here
    N = size(slowness, 1)^0.5;
    slowness_map = reshape(slowness, [N, N]);
    slowness_map = arrayfun(@(x) 1/x, slowness_map);
    slow_min = 1 / max(slowness);
    slow_max = 1 / min(slowness);
%     slow_min = min(slowness);
%     slow_max = max(slowness);
    
    if slow_max - slow_min > eps
        % scale to [1e-6..1-e1]
        low_bound = 1e-6;
        high_bound = 1e-1;
        scale_f = @(x) (x - slow_min) / (slow_max - slow_min) * (high_bound - low_bound) + low_bound;
        slowness_map = arrayfun(scale_f, slowness_map);
    end
    
    tx_num = size(rx_indexs, 1);
    rx_num = size(rx_indexs, 2);
    
    paths_num = tx_num * rx_num;
    M = paths_num;
    
    G = zeros(M, N * N);
    G_counter = 1;
    disp('start raylength matrix calculating...'); tic;
    
    for tx_id = 1 : tx_num
        add_to_tx = false;
        src_point = s_pos(:, tx_id);
        if src_point(1) == 65
            src_point(1) = 64; add_to_tx = true;
        elseif src_point(2) == 65
            src_point(2) = 64; add_to_tx = true;
        end
        [distances, Y] = msfm(slowness_map, src_point);
        
        for rx_i = 1 : rx_num
            rx_id = rx_indexs(tx_id, rx_i);
            rx_pos = s_pos(:, rx_id);
            add_to_rx = false;
%             fprintf('tx id = %d,\trx_id = %d\n', tx_id, rx_id);
            if rx_pos(1) == 65
                rx_pos(1) = 64; add_to_rx = true;
            elseif rx_pos(2) == 65
                rx_pos(2) = 64; add_to_rx = true;
            end
            if abs(slow_min - slow_max) < 1e-8
                shortest_line = shortestpath(distances, rx_pos, src_point, 0.2, 'euler'); 
            else
                shortest_line = shortestpath(distances, rx_pos, src_point, 0.2);
            end
%            figure; imshow(distances, []);  hold on; plot(shortest_line(:,2), shortest_line(:,1), 'r');
%            scatter(src_point(2), src_point(1)); scatter(rx_pos(2), rx_pos(1));
%            pause; break;
            shortest_line = [rx_pos'; shortest_line; src_point'];
            for i = 1 : size(shortest_line, 1) - 1
                x1 = shortest_line(i, 2);
                y1 = shortest_line(i, 1);
                x2 = shortest_line(i + 1, 2);
                y2 = shortest_line(i + 1, 1);
                d = norm([dx * (x1 - x2), dy * (y1 - y2)]);
                if add_to_rx && i == 1
                    d = d + dx;
                elseif add_to_tx && i == size(shortest_line, 1) - 1
                    d = d + dx;
                end
                    
                fx = floor(x1); fy = floor(y1);
                px1 = max(fx - 1, 1); px1 = min (px1, N - 1);
                py1 = max(fy, 1); py1 = min(py1, N);
                fpos1 = px1 * N + py1;
                
                
                if fx ~= floor(x2) || fy ~= floor(y2)
                    G(G_counter, fpos1) = G(G_counter, fpos1) + d/2;
                    fx2 = floor(x2); fy2 = floor(y2);
                    px2 = max(fx2 - 1, 1); px2 = min (px2, N - 1);
                    py2 = max(fy2, 1); py2 = min(py2, N);
                    G(G_counter, px2 * N + py2) = G(G_counter, px2 * N + py2) + d/2;
                else
                    G(G_counter, fpos1) = G(G_counter, fpos1) + d;
                end
            end
            
            G_counter = G_counter + 1;
        end
    end
    toc;
    disp('raylength matrix is ready!');   
    

end

