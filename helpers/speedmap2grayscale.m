function [ X ] = speedmap2grayscale( X, do_uint )
    if nargin < 2
        do_uint = true;
    end
     
    X = abs(X);
    x_ = X(X > 1e-16);
    next_min = min(x_(:));
    X(X<1e-3) = 3/4 * next_min;
    s_min = min(X(:)); s_max = max(X(:));
    
    high = 255;
    low = 0;
    scale_f = @(x) (x - s_min) / (s_max - s_min) * (high - low) + low;
    X = arrayfun(scale_f, X);
    if do_uint
        X = uint8(floor(X));
    end
end

