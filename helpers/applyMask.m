function [ out ] = applyMask(input, value )
%function [ out ] = applyMask(input, value )
    load('helpers/mask.mat');

    [m n ] = size(input);
    
    if m == n
        input = reshape(input, [m*n 1]);
    end

    if size(input,1) ~= size(mask,1)
        error('input and mask size must be the same')
    end
    out = input;
    for i = 1 : numel(mask)
        if mask(i) ~= 1
            out(i) = value;
        end
    end
    
    if m == n
        out = reshape(out, [m n]);
    end
end

