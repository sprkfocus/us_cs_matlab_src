function [ G ] = calc_Raylength_tb( slowness, s_pos, tx_idx, rx_indexs, dx, dy )
%calc_Raylength ( slowness, s_pos, rx_indexs, dx, dy )
    [dim1, dim2] = size(slowness);
    if dim1 == 1 || dim2 == 1
        N = size(slowness, 1)^0.5;
        slowness_map = reshape(slowness, [N, N]);
    elseif dim1 == dim2
        N = dim1;
        slowness_map = slowness;
    else
        error('check dimensions');
    end
    slowness_map = arrayfun(@(x) 1/x, slowness_map);
    slow_min = 1 / max(slowness(:));
    slow_max = 1 / min(slowness(:));
    
    if slow_max - slow_min > eps
        % scale to [1e-6..1]
        low_bound = 1e-6;
        high_bound = 1;
        scale_f = @(x) (x - slow_min) / (slow_max - slow_min) * (high_bound - low_bound) + low_bound;
        slowness_map = arrayfun(scale_f, slowness_map);
    end
    
    tx_num = size(rx_indexs, 1);
    rx_num = size(rx_indexs, 2);
    
    paths_num = tx_num * rx_num;
    M = paths_num;
    
    G = zeros(M, N * N);
    disp('start raylength matrix calculating...'); tic;
    
    for tx_i = 1 : tx_num
        tx_id = tx_idx(tx_i);
%         if  mod(tx_id, 20) == 0
%             disp(['tx_id in progress: ' int2str(tx_id)])
%         end
        src_point = s_pos(:, tx_id);

        [distances, S] = perform_fast_marching(slowness_map, src_point);
        
%         if tx_i == 1
%             figure; imshow(distances, []);  hold on;
%         end
        for rx_i = 1 : rx_num
            rx_id = rx_indexs(tx_i, rx_i);
            rx_pos = s_pos(:, rx_id);
%             fprintf('tx id = %d,\trx_id = %d\n', tx_id, rx_id);

            shortest_line = compute_geodesic(distances, rx_pos)';
%             if tx_i == 1
%                 plot(shortest_line(:,2), shortest_line(:,1), 'r');
%             end
%            figure; imshow(distances, []);  hold on; plot(shortest_line(:,2), shortest_line(:,1), 'r');
%            scatter(src_point(2), src_point(1)); scatter(rx_pos(2), rx_pos(1));
%            pause; break;
            G_line_idx = (tx_i - 1) * rx_num + rx_i;
            for i = 1 : size(shortest_line, 1) - 1
                x1 = shortest_line(i, 2);
                y1 = shortest_line(i, 1);
                x2 = shortest_line(i + 1, 2);
                y2 = shortest_line(i + 1, 1);
                d = norm([dx * (x1 - x2), dy * (y1 - y2)]);
                fx = floor(x1); fy = floor(y1);
                px1 = max(fx - 1, 1); px1 = min (px1, N - 1);
                py1 = max(fy, 1); py1 = min(py1, N);
                fpos1 = px1 * N + py1;
                
                
                if fx ~= floor(x2) || fy ~= floor(y2)
                    G(G_line_idx, fpos1) = G(G_line_idx, fpos1) + d/2;
                    fx2 = floor(x2); fy2 = floor(y2);
                    px2 = max(fx2 - 1, 1); px2 = min (px2, N - 1);
                    py2 = max(fy2, 1); py2 = min(py2, N);
                    G(G_line_idx, px2 * N + py2) = G(G_line_idx, px2 * N + py2) + d/2;
                else
                    G(G_line_idx, fpos1) = G(G_line_idx, fpos1) + d;
                end
            end
            
        end
    end
    toc;
    disp('raylength matrix is ready!');   
    

end
