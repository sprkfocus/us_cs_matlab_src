clear all;

use_receivers = 25;  
used_sensors_num = 100;
save_data = false;


% best x = -1.04302e-06   0.00183072  3.66756e-09  8.15307e-07       11.341
t_shift = -1.04302e-06;  % -6.2711e-07;
sensor_threshold = 0.00183072;  % 0.0038232;
lamda =  1e-10; %  3.66756e-09; %  5.8476e-09; %  7.1995e-10;    
lamda_tv = 1e-6 ;  %8.15307e-07;  % 4.5409e-07;  % 8.7856e-7;  
outmaxiter= 16;  %9 



% arrival_time(z_start, z_end, true, t_shift, sensor_threshold);
load('sim_res/100/arrivals_z0_25s.mat');

m=65; n=65;                     
Length= 6e-2;    
Width=  6e-2;              
d_x=Length/(n-1);d_y=Width/(m-1);
speed_init = 1500;

data_dir = ['sim_res/' int2str(used_sensors_num) '/'];
ending = 's.mat';
load([data_dir 'sensor_pos.mat']);
sensors_pos = sensors;
clearvars sensors;
sensors_pos(1,:) = (sensors_pos(1,:) + Length/2) / d_x;
sensors_pos(2,:) = (sensors_pos(2,:) + Width/2) / d_y;
s_min = min(sensors_pos(:)); s_max = max(sensors_pos(:));
scale_f = @(x) (x - s_min) / (s_max - s_min) * ((m - 1) - 1) + 1;
sensors_pos = arrayfun(scale_f, sensors_pos);


N = 64;

volF = zeros(N, N, z_count);
X_effective = zeros(3196, z_count);

load('X_model.mat');

error = zeros(used_sensors_num, z_count);
psnr_error = error;
snr_error = error;
error_to_full = error;
used_time = error;

tx_custom = 1: 5 : 100;

load('X_full.mat'); 
load('mask.mat');

vol_full = volF(:,:,3);
model = X_model(:,:,3);



 
for use_tx = tx_custom  % 1 : used_sensors_num
    disp('--------------------------------------------------------------------------------');
    disp('');
    disp(['using tx: ' int2str(use_tx)]);
    for z_i = z_start : z_end
        
%         disp(['>>>>Z slice in progress:' int2str(z_i) ]);
        
        load([data_dir 'arrivals_z' int2str(z_i) '_' int2str(use_receivers) ending]);

        % make some changes w/ paths
        
        
        [t_sifted, rx_i_sifted, tx_idx] = sift_data(arrival_t, rx_indexs, use_tx, false);

        tic;
        [volF(:,:, z_i), X_effective(:, z_i)] = process_slice_param(t_sifted, rx_i_sifted, tx_idx, ...
            sensors_pos, lamda, lamda_tv, outmaxiter);
        used_time(use_tx, z_i) = toc;
        
        % Error calculation
        
        result = volF(:,:, z_i);
        
        error(use_tx, z_i) = norm(result - model , 'fro');
        error_to_full(use_tx, z_i) = norm(result - vol_full, 'fro');
        
   
        [psnr_error(use_tx, z_i), snr_error(use_tx, z_i)] = psnr(result, model);
        
        if use_tx == 66
           map_sixty = volF(:,:,z_i); 
        end
        
       
        toc;
    end
end
    
return
    z_i = 3;
    F = volF(:,:, z_i);
   
%     XE0 = X_effective(:, z_i);
    % postprocessed images analysis     
   
    
    wav = daubcqf(4);
    W = @(x) mdwt(x,wav);  
    WT = @(x) midwt(x,wav);
    Psi = A_operator(@(x) WT(x), @(x) W(x));  
    
    X = Psi' * F;
%     mesh(X);

    norm_len = zeros(1, 25);
    
   
    
    s = 13;
    for s_ = 1 : 25
        x_cut = X;
        x_cut(s_ + 1:N, s_ + 1 : N) = 0;

        F_re_ = midwt(x_cut, wav);

        norm_len(s_) = norm(F_re_ - F, 'fro');
        if s_ == s
            F_re = F_re_;
        end
    end
    plot(1:25, norm_len);
    
    
    subplot(221); imagesc(F);
    subplot(222); imagesc(F_re);
    subplot(223); plot(norm_len);
    
    load([data_dir 'arrivals_z' int2str(z_i) '_' int2str(use_receivers) ending]);
    Y = arrival_t; clearvars arrival_t;
    Nl = N^2;
    m = 4*s* log(Nl/s)
    m = ceil(m);
    M = size(Y, 1);
    rng('default'); rng(2016);
    ids_to_take = randperm(M);
    ids_to_take = ids_to_take(1 : m);
    A = zeros(m, M);
    for i = 1 : m
       A(i, ids_to_take(i)) = 1; 
    end
    
    y = A * arrival_t;
    
    load('temp_G0.mat');

    
    return;
    
    
   
%     Xvis = X;
%     for z = 1 : 15
%        Xvis(:,:,z) = speedmap2grayscale(Xvis(:,:,z), false); 
%     end
%        
%     figure;
%     hold on;
%     [ax, ay, az] = meshgrid(1:N,1:N,1:z_count);
%     for a = 10.^(1:4) % 'a' defines the isosurface limits
%         p = patch(isosurface(ax,ay,az,Xvis,max(Xvis(:))/a)); % isosurfaces at max(V)/a
%         isonormals(ax,ay, az,X,p); % plot the surfaces
%         set(p,'FaceColor','red','EdgeColor','none'); % set colors
%     end
%     alpha(.3); % set the transparency for the isosurfaces
%     daspect([1 1 1])
%     view(3), axis vis3d tight, box on, grid on
%     camproj perspective
%     camlight, lighting phong, alpha(.5)




  
    
    
    

