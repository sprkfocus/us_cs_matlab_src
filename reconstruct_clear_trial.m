function [slice] = reconstruct_clear_trial( do_bigdata, rx_indexs_adv, ...
                                        cs_A, do_use_full_bentray, ...
                                        lamda, lamda_tv, t_step_init)
% function straightpath_congd20151021_3
    include_subpaths();
    if ~exist('lamda', 'var') || (lamda == 0)
        lamda= 3.66756e-09;          
        lamda_tv=8.15307e-07;  
        t_step_init = 1;
    end
    if exist('do_bigdata', 'var') && do_bigdata
        data_dir = ['sim_results/1024/'];
        Length= 20e-2;    
        Width=  20e-2; 
    else
        data_dir = ['sim_results/wave_s1/'];
        Length= 4e-2;    
        Width=  4e-2;    
        do_bigdata = false;
    end
    if ~exist('use_full_bentray', 'var')
        do_use_full_bentray = false;
    end

    
    outmaxiter = 25;
    m=65; n=65;                     
          
%     d_x=Length/(n-1);d_y=Width/(m-1);

    
    load([data_dir 'sensor_pos.mat']);
    sensors_pos = sensors_pos_abs2grid(sensors, Length, Width, m, n);
    
    
    load([data_dir 'arrivals_t.mat']);
 
    if exist('cs_A', 'var') && max(size(cs_A)) > 0
       arrival_t = cs_A * arrival_t; 
    end

    speed_init = 1500; % m/s
    slowness_init=1/speed_init;
    

    N=3196; %  effective cells
    SS0=slowness_init.*ones(N,1);  

    psi_filename = ['cached/psi_' int2str(N) '.mat'];
    if exist(psi_filename, 'file')
        load(psi_filename); 
    else
        psi_=orth(rand(3196, 3196));
        save(psi_filename, 'psi_');
    end
    
    init_raylenth_file = ['cached/G_init_' int2str(size(arrival_t, 1))  '_' int2str(N)  '.mat'];
    load_paths = 2 == exist(init_raylenth_file, 'file');

        
    if ~exist('rx_indexs_adv', 'var')
       rx_indexs_adv = rx_index_input2advanced(rx_indexs);
    end
    
    load('helpers/SS_res.mat'); 
    
    M = (m - 1)*(n - 1);
    
    Tol_grad=1e-16;    

    sigma=zeros(N);      
    iter_cond=1;                
    alpha=0.01;beta=0.5;  
    t_=t_step_init;                
    n0=0;     

    while (iter_cond)
        disp(['reconstruct iter: ' int2str(n0) ' / ' int2str(outmaxiter) ]);  
        SS_cur = SS0;      
        psiT_slow = psi_' * SS_cur; 

        for ii=1:N
            sigma(ii,ii)=sqrt((psiT_slow(ii))^2+eps);
        end
        
        if (n0 == 0 && load_paths)
            load(init_raylenth_file);
            if size(G0, 1) ~= size(arrival_t,1)
                clearvars G0;
            end
        end
        if ( (n0 == 0 && ~exist('G0', 'var')) || do_use_full_bentray )
            disp('start raylength matrix calculating...'); 
            calc_start = cputime();
            slowness_map = ones(M, 1).*slowness_init;  %./100;
            num_SS_res=numel(SS_res);
            for jj=1:num_SS_res
                slowness_map(SS_res(jj))=SS0(jj);
            end
            dx = Length / (m - 2);
            G_t = calc_Raylength_tb_eval(slowness_map, sensors_pos, rx_indexs_adv, dx, dx);
            G0 = zeros(size(G_t, 1), N);
            for p_id = 1 : size(G_t, 1)
                for jj=1 : num_SS_res
                    G0(p_id, jj) = G_t(p_id, SS_res(jj));
                end
            end
            disp([' > raylength matrix is ready! Cputime elapsed: ' num2str(cputime() - calc_start) '[s]']);
            if n0 == 0 && ~load_paths
                save(init_raylenth_file, 'G0'); disp('G0[0] has been saved'); 
            end
        end

        if n0 == 0
            G_prev = G0;
        end



%         C0(:,n0+1)=norm((G0*SS(:,n0+1)-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS(:,n0+1));  %Ŀ�꺯��
        r0=G_prev * SS_cur-arrival_t;                  
        grad_C0=2*G0'*r0+lamda*psi_*1/(sigma)*psiT_slow+lamda_tv*grad_1norm_tv(SS_cur);    %Ŀ�꺯����ݶ�
        grad_C1=grad_C0;              %grad_C1���ÿ�ε��ݶ�


        if  n0==0
            gamma_1=0;                                  %gamma_1������еĲ���
            delta_m_1=-grad_C0;                 %delta_m_1�����

        else
    %         gamma_1 = norm(grad_C0(:, n0 + 1) ,2 )^2 / norm(grad_C0(:, n0) , 2)^2;
            yta = grad_C1-grad_C0_prev;
            gamma_1=max(0,min((grad_C1'*yta)/(delta_m_0'*yta),norm(grad_C1,2)^2/(delta_m_0'*yta)));
            delta_m_1=-grad_C1+gamma_1*delta_m_0;
        end

           %��backline search Ѱ�Ҳ���u_n
        while (norm((G0*(SS_cur+t_*delta_m_1)-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS_cur))>...
               (norm((G0*SS_cur-arrival_t),2)^2+lamda*norm(psiT_slow,1)+lamda_tv*f_1norm_tv(SS_cur)+alpha*t_*real(grad_C1'*delta_m_1))
           t_=beta*t_;
        end

        u_n0=t_;
        if u_n0 < 1
           disp(['mu =' num2str(u_n0)]) 
        end
        SS1=SS0+u_n0*delta_m_1;                 
        f=norm(u_n0*delta_m_1,2)/norm(SS0,2);   

        if (f>Tol_grad)&&(n0<outmaxiter)
           iter_cond=1;
        else
           iter_cond=0;
        end

        G_prev = G0;
        grad_C0_prev = grad_C0;
        delta_m_0=delta_m_1;
        SS0=SS1;
        n0=n0+1;
        t_=t_step_init;

    end

    SS_effective=SS1;

    num_SS_res=numel(SS_res);
    SS_final=slowness_init*ones(M,1);
    for jj=1:num_SS_res
       SS_final(SS_res(jj))=SS_effective(jj);
    end

     
   VDOTMN=zeros(m,n);  
   
    for k=1:M
        I=floor(k/(n-1));
        J=mod(k,(n-1));
        if (I~=0)&&(J~=0)
            VDOTMN(I:I+1,J:J+1)=1/SS_final(k);
        elseif(I==0)&&(J~=0)
            VDOTMN(1:2,J:J+1)=1/SS_final(k);    
        elseif(I~=0)&&(J==0)
            VDOTMN(I:I+1,n-1:n)=1/SS_final(k);
        end
    end
           
    slice = VDOTMN(1:64,1:64);
end


          
          
