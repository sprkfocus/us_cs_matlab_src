% Main script for the evaluation
mkdir('cached')
include_subpaths();
% generate Psi
psi_filename_adhoc = 'cached/psi_3196.mat';
if ~exist(psi_filename_adhoc, 'file')
    disp('Generating PSI...');
    psi_=orth(rand(3196, 3196));
    save(psi_filename_adhoc, 'psi_');
end

speed_model = simulated_model_1();
disp('k-wave calculations...');
disp(''); disp('Reconstruction 1: K-wave data, regular reconstruction');
tic();
kwave_slice_clear = reconstruct_clear(false);  
clear_elapsed = toc();


% cs_s = cs_analysis(slice_clear, false);
cs_s = 11^2;
cs_N = size(speed_model, 1)^2;
cs_m = 4 * cs_s * log(cs_N / cs_s);
disp(''); disp('Reconstruction 2: K-wave data, CS reconstruction');
tic();
[kwave_slice_cs, used_data_ratio] = reconstruct_cs_classic('no_line_comb', 'cs', cs_m);
cs_elapsed = toc();
disp('--------------------------------------------------------------------------------');
disp('> K-wave data');
disp('Regular reconstruction:');
disp(['    Time elapsed: ' num2str(clear_elapsed) ' [s]']);
disp('Compressive Sensing reconstruction:');
disp(['    Time elapsed: ' num2str(cs_elapsed)]);
disp(['    Amount of the data in use for CS: ' num2str(used_data_ratio * 100) '%']);
disp('--------------------------------------------------------------------------------');

figure;
subplot(131);
[img_ref, x_arg, y_arg] = make_picture(speed_model);
imagesc(x_arg *1000, y_arg *1000, img_ref);
xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('Model (K-Wave)');
hold on;

subplot(132);
[img_basic, x_arg, y_arg] = make_picture(kwave_slice_clear);
imagesc(x_arg *1000, y_arg *1000, img_basic);
xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('Original reconstruction algorithm (K-Wave)');

subplot(133);
[img_cs, x_arg, y_arg] = make_picture(kwave_slice_cs);
imagesc(x_arg *1000, y_arg *1000, img_cs);
xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('CS reconstruction algorithm (K-Wave)');


disp('PZFlex data skipped');
return;


% -------------------------------------------------------------------------
% ----------  pzflex ------------------------------------------------------
% -------------------------------------------------------------------------


disp('pzflex calculations...');


disp(''); disp('Reconstruction 3: Pzflex data, regular reconstruction');
tic();
[slice_clear, dummy] = pzf_recon_clear(false); 
clear_elapsed = toc();


% cs_s = cs_analysis(slice_clear, false);
cs_s = 11^2;
cs_N = size(speed_model, 1)^2;
cs_m = 4 * cs_s * log(cs_N / cs_s);
disp(''); disp('Reconstruction 4: Pzflex data, CS reconstruction');
tic();
[slice_cs, used_data_ratio_pzf] = pzf_recon_clear(true);
cs_elapsed = toc();


disp('--------------------------------------------------------------------------------');
disp('> Pzflex data');
disp('Regular reconstruction:');
disp(['    Time elapsed: ' num2str(clear_elapsed) ' [s]']);
disp('Compressive Sensing reconstruction:');
disp(['    Time elapsed: ' num2str(cs_elapsed)]);
disp(['    Amount of the data in use for CS: ' num2str(used_data_ratio_pzf * 100) '%']);
disp('--------------------------------------------------------------------------------');


% subplot(234);
% [img_ref, x_arg, y_arg] = make_picture(speed_model);
% imagesc(x_arg *1000, y_arg *1000, img_ref);
% xlabel('X [mm]')
% ylabel('Y [mm]')
% axis('image')
% colormap(gray(256))
% title('Model (PZFlex)');
% hold on;

subplot(223);
[img_basic, x_arg, y_arg] = make_picture(slice_clear);
imagesc(x_arg *1000, y_arg *1000, img_basic);
xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('Original reconstruction algorithm (PZFlex)');

subplot(224);
[img_cs, x_arg, y_arg] = make_picture(slice_cs);
imagesc(x_arg *1000, y_arg *1000, img_cs);
xlabel('X [mm]')
ylabel('Y [mm]')
axis('image')
colormap(gray(256))
title('CS reconstruction algorithm (PZFlex)');
hold off;

