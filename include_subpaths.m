if isempty(strfind(path,'./helpers'))
    addpath('./transforms');
    addpath('./helpers');
    addpath('./cached');
    addpath('./modeling');
end