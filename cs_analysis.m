function [ best_s ] = cs_analysis( slice, do_plot )
    N = size(slice,1);
    wav = daubcqf(6);
    W = @(x) mdwt(x,wav,6);  
    WT = @(x) midwt(x,wav,6);
    Psi = A_operator(@(x) WT(x), @(x) W(x));  
    
    X = Psi' * slice;
    
%     x = X(:,1);    
    best_s = best_s_line(X(:, 1));
    lines_to_investigate = floor(sqrt(best_s));
    for i = 2 : lines_to_investigate
       best_s = best_s + best_s_line(X(:, i)); 
    end
    best_s = round(best_s / lines_to_investigate);
          
    disp([' > best S: ' int2str(best_s)]);
     
    if do_plot
        figure;
        subplot(231);
        plot(X(1,:));
        hold on; 
        subplot(232);
        plot(er_x);

        subplot(233);
        imagesc(slice);
        title('Original slice');

        subplot(234);
        X_compressed = X;
        X_compressed(best_s : N, best_s : N) = 0;
        imagesc(Psi * X_compressed);
        title(['Compressed with m = ' int2str(best_s)]);

        subplot(235);
        X_compressed = X;
        X_compressed(best_s_next : N, best_s_next : N) = 0;
        imagesc(Psi * X_compressed);
        title(['Compressed with m = ' int2str(best_s_next)]);
        hold off;   
    end
   
    best_s = best_s^2;

end

function [s] = best_s_line(x)
    %energy ratio -- method --
    x_len = size(x,1);
    er_x = zeros(x_len, 1);

    ne = 10;

    for i = 1 : x_len
         er_up = 0;
         for j = max(1, i - ne) : i
             er_up = er_up + x(j)^2;
         end
         er_down = 0;
         for j = i : min(x_len, i + ne)
             er_down = er_down + x(j)^2;
         end
         er_x(i) = er_up / er_down;
    end

    er_x(x_len - ne - 1 : x_len) = 0;
    for i = 1 : x_len
        er_x(i) = (abs(x(i)) * er_x(i))^3 ;
    end

    max_energy = max(abs(er_x));
    for i = flip(1 : x_len)
         if abs(er_x(i)) > 0.01 * max_energy
             s = i;
             break;
         end
     end
end

